
.. page 29

III. WHAT IS MODERN MONEY?
--------------------------

A MONETARY system should, as we have seen, distri-
bute communally produced wealth for individual
consumption and use. With our monetary system,
this, which should be the only purpose, has been
reduced to a secondary, incidental and almost acci-
dental role. The primary purpose for which money
is issued in this country is to lend so as to make
it bear interest. The ruling passion that makes
all men misers has over-reached itself here in a
theoretically highly curious way though the results
are tragic enough in practice.

The general ideas and modes of reasoning, which,
in the nineteenth century, resulted in that great
clarification of the physical sciences, by which prac-
tical engineers have been able to solve, to all intents
and purposes, the problems of production, have
been suitably applied by the author to elucidate
the nature of money. In negative form they are
expressed by the Latin motto "Ex nihilo nihil fit",
from nothing nothing comes. In positive form they
are known as the laws of conservation of matter
and energy, a general knowledge of which is now
as widely diffused as the knowledge of reading and
writing.

.. page 30

It is true that twentieth century science, since
Einstein's Theory of Relativity, attempt the fusion of
the two laws into one. It is possible that energy and
matter may be equivalent at a definite exchange ratio,
just as Joule showed, a century ago, was the case
for heat and work or any other form of energy. If
so, one law will do for both.[*]_ But this it as yet only
a view, for the actual conversion of the one into the
other has yet neither been proved to occur, nor can
it be practically carried out. Whether true or not, it
does not affect the argument. There it altogether too
much transcendental mathematics about money as it is.

.. [*] Compare, for example, *The Mysterious Universe*, Sir J.
       Jeans, 1930, Camb. University Press.

The common sense of these laws it that a perpetual
motion machine it not possible. To produce wealth
energy must be expended or consumed. Consumption
here does not, of course, mean total destruction,
any more than when we speak of the consumption
of fuel or food, but conversion into useless waste
products. The product of the consumption of energy
is heat of temperature too low to be of use. It
is not of fundamental importance, though it may
be practically very important, whether the work
required to produce wealth comes from a man, a
donkey-engine or any other prime-mover. We
have seen that this age has been made possible by
the substitution of inanimate energy for animal
labour in production. A single machine to-day
can undertake as much actual labour at a com-
munity of a million labourers working in three shifts
of eight hours a day.

.. page 31

To this common sense truism that, in the world
ruled over by physical laws, it is impossible to get
something for nothing, modern money is an apparent
outstanding exception, the elucidation of which
throws a flood of light on the nature of money and
suffices to rob it for ever of its robe of mystery -
and let us hope, of its power of evil. Money to
most people still conveys the idea of coins, but,
except as small change, coins are obsolete. In so
far as modern money has any tangible physical
existence, and by far the greater part has none,
it is a paper token, which like a postage stamp,
costs next to nothing to make, and which has some
value printed on it. Its owner for the time being
is entitled to that amount of wealth in exchange
for it. Strictly he is empowered by the law to make
any creditor take it as legal tender for that amount
of debt, which in practice comes to exactly the same
thing.

.. page 32 in 13

But most money nowadays has no existence except
as a statement of account or bank-balance upon
which the owner draws by cheque. The difference
between the amounts paid in and paid out is a
*sum of money* which the owner owns, but the money
does not exist. What is paid out of one account
is paid into another, and this is done as a book-entry
without any money at all. A relatively small amount
of paper tokens suffices for the owners who need
"cash". The cheque system is in itself a great
advance upon the use of tokens in many ways.
But its invention has resulted in the banks, not
indeed coining money as that is quite unnecessary.
but of creating money, without even the issue of
printed notes, which they lend at interest to those
who will pay interest on the pretended "loan".

Coins, in reality are tokens like paper money,
but differ in that the token itself is not entirely
valueless. If melted and sold, the bullion or scrap
metal would fetch the whole or part of the nominal
value of the coin, if bartered for other goods. But this
is an offence, so that even if they are made of prec-
ious metals of full value they are tokens merely. Since
the War, gold coins, which contained a weight of
gold equal in value to the coin, have ceased to be
current, and the quantity of silver in the alloy used
for silver coins is now but a small fraction of their
value. They are a curious relic of a once public
currency, for they are still issued by the State Mint,
and carry the image and superscription of the reign-
ing Sovereign. But under democracy the prerogative
of the issue of money has been usurped by private
financial companies, and the State money is re-
duced to a trivial proportion of the whole.

So far as the point referred to is concerned, it
matters not at all whether the money in question
is issued by the State, a bank or a counterfeiter.

*The issuer of money, who first puts it into circulation,
cannot help getting something for nothing, namely the
exchange value of the money.*

.. page 33 in 5

So, not in the twentieth century to make a pretence
that there is anything really mysterious or miraculous
about the operation, or that practical economics
has a special dispensation from the laws of conserva-
tion, the problem is to find where this wealth comes
from. The quantity is not inconsiderable. In this
country it is equivalent to well over £2,000,000,000.
To answer the question, it is only necessary to
contrast a purely hypothetical community, without
a monetary system, exchanging its goods by barter,
with our present system, or, better, to presuppose
the first condition and from it evolve our own.

It might occur in some such way as this. A has
leather and B has flour. A wants flour and B,
though he does not at the moment require leather,
has more flour than he needs and is willing to supply
A in exchange for a simple I O U, a paper note
expressing the fact that" I, A, owe you, B, so much
---." The quantity owed might be either flour
or leather, if B had indicated he would later be
requiring leather. But if, more generally, the note
ran "I, A, owe you, the owner of this note, goods
equal in barter value to so much flour", B, if he
wanted, say, nails, could exchange it with C, who
produced nails, and thus confer upon C the right,
at his own convenience, of exchanging it with A,
for flour. A would then destroy the I O U he
had given. Not only A, but B, C . . . and the
whole business community might get into the habit
of doing business in this way.

.. page 34 in 7

The difference between such a system and a modern
money system is simply in the growth of the under-
standing or convention that the I of the I O U is
not anyone individual, but all the individuals, who
agree to accept such notes as "legal tender" for
payment of debts and that, in countries on the
gold standard, the quantity owed is some definite
weight of gold. Then instead of A tearing up the
note he had given when he recovered it - as it before
expressed the entirely redundant information how
much A, as the holder of the note, owes himself -
and B, C . . . doing the same with their notes in
similar circumstances, they do not tear them up,
but pass them on *for ever and ever*.

We may pause to enquire at this stage the reasons
which make the community prefer such notes to
goods, which are identical with those which in this
country cause people *not* to own over £2,000,000,000
worth of goods, but prefer claims to them, which,
as regards any individual, could instantly be
exchanged upon demand for goods. It is simply
that they do not know either what goods they will
want next or in what quantities. Also they do
not want to possess more than they need because
it rots, or, at any rate, costs them something to look
after. Production being communal, individuals
have to *go without* part of the wealth they are entitled
to for money, that is they must have valid claims
upon the goods of the community for sale in the market
in order to obtain what they need as they need it.
*It is this wealth the issuer of money gets for nothing.*

.. page 35 in 8

Clearly once a monetary system is got into operation
it will function indefinitely, the money circulating
from one individual to another without ever being
called in again, and it will create indispensable
facilities for the exchange and distribution of goods
and services. But *how* is it started? There must
be some central authority, bank or counterfeiter,
to issue the money and receive in exchange the
wealth the citizens are compelled to give up for
nothing but the paper claim to it. So far as the
accounting goes, it is clear that the citizens are
owed for what they have given up. The interesting
point is "Who owes for it?" Clearly it is *not*
the issuer of the money, for it is in the nature of the
transaction that if he repaid the citizens back the
goods they give up, it puts the money out of circu-
lation and reduces the issue to an absurdity. The
citizens do not want to return to barter, but they
do want the goods back in exchange for their money
*from one another*, those with money and no goods
wanting goods from those with goods in exchange
for money.

.. page 36 in 18

So that it is clear that money is a peculiar and
special form of debt, quite different from those
contracted by people who lend in return for periodic
interest payments and the ultimate repayment of the
principal. It is a form of national debt, owned by
individuals, which is legal tender in exchange for
the goods of any citizen who wishes to sell them, and
then in turn becoming legal tender for what the
new owner wishes to buy. Clearly the profits of
the issue of money should belong to the community.
A counterfeiter issuing money is punished if convicted
for treason rather than for theft. But the banks,
by the cheque system, have invented a means of
issuing money without coining it or even issuing a
bank note, and this form of money in quantity makes
the whole of the rest insignificant. They have
received in exchange for it the community's goods,
which have been consumed by the persons to whom
they "lent" the money, and they are owed by these
borrowers for what the community bas been com-
pelled by the issues of money to give up. The money
so issued passes into the hands of private individuals
and is owed to them by the banks as a current or
cheque account. But the banks have not the money, as
it has no physical existence, and could not repay
it if it were demanded. They hold only the securities
deposited with them as cover by those who in turn
owe the money to them.

The nature of modern money and how it comes
into existence will be clear to the uninitiated from
the former description, so far as concerns a paper
note. In the case of the non-existent money of
bank balances, the expense of printing or forging
the notes, and of maintaining them in repair in circu-
lation, is saved. But there is another and far more
important and sinister reason why this form of
money is now the chief part of the total. The periodic
printing and destruction of hundreds of millions of
money in the form of notes in the full light of day
in the interests of industry might occasion outbursts
from the buying and selling public alternately,
rightly afraid of their money being debased in value,
or of their businesses being ruined, as the case may
be.

.. page 37 in 6

The great and sinister advantage possessed by a
bank balance over any actual physical tokens is
that it may be negative instead of positive without
the public knowing what is really taking place,
by arrangement between the bank and the current
account holder. The banker allows a client, willing
to deposit security and pay interest on the accommo-
dation at the bank-rate, to *overdraw* his account.
This is one of the common methods by which a
bank, in contradistinction to an individual, advances
a "loan". But unfortunately for this interpretation
of the transaction, a real loan does not challenge the
law of conservation. There is no magic about it.
What the borrower receives, the creditor gives up.

But the transaction considered has just that remark-
able feature which arrested our attention as the
invariable result of the issue of new money. No
one consents to give up, or even knows that he has
given up what the borrower gets in this case. The
latter is empowered by the overdraft to go to the
market and receive goods or services from individuals,
without, as in a genuine sale or loan, the money with
which he buys having been given up by somebody
else. Before the transaction, the public owned a
certain amount of money. After the transaction
they own this plus the amount of the overdraft. It
goes to increase the current accounts of those who
sell goods to the person who receives the overdraft,
and it is not taken out of any existing account.

.. page 38 in 9

It should require no great effort to-day for any-
one after the experiences of the variations of money
during the War, to master the calculation involved.
Let us suppose that before receiving an overdraft
of any sum, say £A, a borrower owns £\ :sub:`0` and the
nation £X. After the borrower spends his over-
draft - he would not borrow and pay interest on
it to keep it tied up in his stocking - he owns £\ :sub:`0`,
owes £A, and the nation own £(A + X). The
debt in goods which the individuals of the nation
owe among themselves instead of being divided
among £X is now divided among £(A + X),
with the consequence that each £\ :sub:`1` is now worth
in goods less than before as X : (A + X), or the
price-index rises in the ratio (A + X) : X. That
is to say, instead of people thinking always that
their goods are dear they should look upon it the
other way, that their money is being debased, but
more cunningly than was known to the notorious
monarchs who used to put more base-metal into
the coins.

It is always, of course, argued that the borrower
actually does give up securities to the bank of greater
value than the amount of the "loan". Actually
he gives up nothing whatever. All that happens
is that his securities are kept in the bank's safe
instead of his own to protect *the bank* IF he should
default. In this case the bank *then* sell his security
and thus withdraw again the money they have
issued. If the bank sold the security before they
advanced the money there would be no new issue
of money. But the owner can do this just as well
himself without the bank. That is the trouble,
for it means that of the three possible ways in which
this great nation can get the money to carry on
its services, from the State, the banks and individual
counterfeiters, only the first now remains possible.

.. page 39 in 2

The facts are well known and in no dispute
whatever. "Every loan makes a deposit"[*]_ is a
sinister phrase invented by apologists for the system,
which has been repeatedly endorsed by the Rt.
Hon. R. McKenna, Chairman of the Midland
Bank. The explanation, of course, is that what is
called by the banker a "loan" is no loan at all
but a creation of new money, if the "loan" is an
entirely new one, or a continuation in existence of
money previously so created if the "loan", having
been repaid, is extended to another client. If it
is not extended to someone else, then the money
issued, when the "loan" is made, is destroyed
again when the "loan" is repaid. Every such
new issue of money creates a deposit in somebody's
account. That is easy to understand. But the
attempt to camouflage the operation by calling it a
"loan" seems as pitiful as the stock plea of the
defaulting embezzler when detected, which the
judge trying a recent case so sternly held up to
public obloquy.

.. [*] *The Meaning of Money,* p.63, Hartley Withers, 1922, John
       Murray.

.. page 40 in 13

In this way, well over £2,000,000,000 worth of
wealth has been taken from the public and is owed
to the banks by those who have borrowed it, and
by the banks to their clients. In return, the banks'
clients have their bank accounts often kept for
nothing, and, if a national money system were
adopted in place of the present private money
system, presumably people would have to pay for
having their bank accounts kept, as they have for
any other service. From the invention of the
cheque system to 1914, money was expanded by
these means to the total sum of £1,200,000,000
of which at least one thousand million pounds was
of private creation. During and after the War it
was relatively suddenly multiplied to more than
double and, in 1920, was £2,700,000,000. The
value of the £\ :sub:`1` in goods fell almost in proportion,
and became worth only about 8/- pre-War value.
A few hundred millions only of the increase were
due to national money in the form of £\ :sub:`1` and 10/-
Treasury notes. By the Bank Note and Currency
Act 1928 under the last Government these were
suppressed and replaced by Bank of England Notes.
So that at the present time, out of a total of well
over two thousand million pounds. only same fifty-
eight millions are National or State-issued money.[*]_


.. [*] *This age of Plenty*, Appendix, A, C. Marshall Hattersley,
       M.A., LL.B., 1929, Sir Isaac Pitman & Sons, Ltd.

Before the War the new issue of money by the
banks were limited by the right of the public to
demand gold in exchange for paper money. Gold
to the extent of a few per cent. had to be held in
the country as a "backing" for the money privately
issued, and if the value of money fell and prices rose
so that it was cheaper to send abroad gold in settle-
ment of debts rather than other goods, the drain
of gold reduced the basis of "credit", and deflation
or reduction of the total money was effected by calling
in "loans" and not extending them. Since it is
quite impossible to issue money on such principles
without causing alternate rise of prices and consequent
deflation and industrial depression, the history of
the system has been one of boom following slump
with the periodic regularity that has earned the
special title of the "Trade Cycle".

.. page 41

It must not for a moment be supposed that by
this system the money of the country and its standard
of prosperity was increased as rapidly as it would
have been under a scientific monetary system. The
truth is that here as in many other directions the
prodigality of science is such that progress was made
*in spite of* antiquated laws, anti-social practices, and
the amputation of the financial from the political
government, which would have ruined any poorer
age.

.. page 42 in 22

During the War, the right of the public to demand
gold money was taken away, and the so-called
"Gold-Standard Act of 1925" did not reinstate
it, so far as the public is concerned. But persons
engaged in foreign trade were thereby empowered
to buy whole bars of gold of some 400 troy oz.
weight for paper at the full pre-War value of the
£\ :sub:`1` sterling in relation to gold. The ostensible object
of this attempt to return to a gold standard was
to raise the value of the £\ :sub:`1`, which by the inflation
during the War had sunk in value to less than half,
back again to its pre-War amount. The many
attendant evils that were bound to follow from, and
have followed from this course, were perfectly well
known to students of the subject as soon as it was
mooted, and eight years before the public, who
are now beginning to grow alarmed at the effects,
understood what was proposed. It was known
that prolonged industrial depression and grave
increase of unemployment were bound to follow
as the night the day, and that, if the attempt suc-
ceeds, the real burden of the National Debt, the
interest upon which amounts to over a million
pounds a day, would be magnified in the same
ratio as the value of the £\ :sub:`1` in goods, was increased.
It would seem almost impossible that this nation can
be strong enough to survive the disabilities and
burdens that are being piled upon it, ostensibly
in the interests of "sound finance", really in the
attempt to make two unsounds sound.

It is no answer to say that these consequences,
indefensible as they are, were the natural result
of the War. The financial measures instantly put
into operation on the outbreak of the War were,
of course, prepared to their minutest detail before.
This kind of finance, by paying for the cost of
preparation for larger output necessitated by war,
out of the value of the existing money enables war
to be financed without the peoples' consent, and
that is no doubt one of the attractions. If the
people could be relied upon to consent to the extent
of paying honestly for the additional expenditure
incurred, there is no reason why even wars should
not be waged without debasing the unit of monetary
value.

.. page 43 in 9

It was hoped that the Act would conserve the
gold supplies, but on Sept. 3rd, 1930, they were
£220,536 less than at the passing of the Act. The
discrimination in favour of the foreign trader, when
our home industries were so depressed, was probably
incidental to the pursuit of the larger policy of
increasing the War burden, rather than purely
gratuitous. It has been submitted that the reason
why the cheap sixpenny shops flourish in this country
so amazingly is that they can sell foreign goods
below any possible cost of production by changing
the paper they receive for gold and buying with
gold abroad.[*]_


.. [*] *The Next Budget*, Junius Junior, 1930, Cecil Palmer.

Whether this is well-founded or not, it would
seem undeniable that the present value of the £\ :sub:`1`
has not been raised in value to the home-producer,
but has to the importer. If this is not being taken
advantage of by business men, it must be for patriotic
and altruistic reasons very different from those which
make for success in their walk of life. It would be
more natural to assume that it was being taken the
fullest advantage of, and that the competition our
own workers are being subjected to is in part of this
purely artificial and crooked character.
