================
MONEY versus MAN
================

| .
|
|
|
|
|
|
|
|
| **MONEY versus MAN**
|
| A STATEMENT OF THE WORLD PROBLEM
| FROM THE STANDPOINT OF
| THE NEW ECONOMICS
|
| *by*
|
| FREDERICK SODDY
| M.A., F.R.S.
|
| *Dr. Lee's Professor of Chemistry,*
| *University of Oxford*
| *Nobel Laureate in Chemistry, 1921*
|
|

.. image:: img/publisher.png
   :width: 350
   :alt: Publisher
