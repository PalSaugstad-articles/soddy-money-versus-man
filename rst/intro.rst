.. page VI

About this version
------------------

This is a 'reconstruction' of the the book MONEY *versus* MAN by Frederick Soddy.
The sources used are these:

`https://www.flickr.com/photos/bn37ej/3435808100/in/album-72157616673992980/lightbox/ <https://www.flickr.com/photos/bn37ej/3435808100/in/album-72157616673992980/lightbox/>`_

`https://dspace.gipe.ac.in/xmlui/handle/10973/22230 <https://dspace.gipe.ac.in/xmlui/handle/10973/22230>`_

The latter consists of scans of the pages plus 'OCR_' of those scans.
Due to the variable quality of those scans, there are many errors in the OCR text output.
By carefully reading through the text several times, most of those errors have been corrected in this version.
The raw text data for this version can be found in this bitbucket project:

`https://bitbucket.org/PalSaugstad-articles/soddy-money-versus-man <https://bitbucket.org/PalSaugstad-articles/soddy-money-versus-man>`_

*Pål Saugstad*

|datetime|


.. _OCR: https://en.wikipedia.org/wiki/Optical_character_recognition

| .
|
|
|
|
|
|
|
| .

"CONSUMPTION absolute is the end, crown and per-
fection of production; and wise consumption is a
far more difficult art than wise production.

Capital which produces nothing but capital is
only root producing root; bulb issuing in bulb,
never in tulip; seed issuing in seed never in bread.
The Political Economy of Europe has hitherto
devoted itself to the multiplication . . . of bulbs.
It never saw nor conceived such a thing as a tulip.

This being the nature of capital it follows that
there are two kinds of true production, always going
on in an active State; one of seed and one of food;
or production for the Ground, and for the Mouth;
both of which are by covetous persons thought to
be production only for the granary; whereas the
function of the granary is but intermediate and
conservative, fulfilled in distribution; else it ends
in nothing but mildew and the nourishment of rats
and worms.

The wealth of a nation is to be estimated only
by what it consumes.

As consumption is the end and aim of production,
so life is the end and aim of consumption.

THERE IS NO WEALTH BUT LIFE."

*Unto this Last.* John Ruskin. 1862.


.. image:: img/economic-cycle3.png
   :width: 350
   :alt: Economic Cycle

DIAGRAM OF THE ECONOMIC CYCLE

*(see p. 64)*
