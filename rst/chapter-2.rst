.. page 18


II. MONEY, WEALTH AND DEBT
--------------------------

WHILE recognising to the full the social changes
that were bound in time to follow the mechanisation
of production, the immediate effects upon the great
mass of the workers could not have been so damaging
and disastrous as they have proved, but for the
complete failure of the monetary system, which till
then had sufficed to deal with the old scale of
production.

.. page 19 in 16

The distributory mechanism of an individualistic
community is its money system. Money distributes
among individuals the revenue of wealth of the
community. No individual to-day is capable of
producing by himself all that he consumes or uses
either in his private life or in the conduct of his
avocation. It is only in the rudest and most primitive
stage that such a condition is even conceivable.
Hence some form of monetary system, or equivalent
distributive mechanism, is the first stage in the
evolution of society from the primitive state. The
production of wealth becomes increasingly communal,
but the consumption and use of it remain individual.
There is no other ultimate purpose but individual
consumption and use in production, and the returning
of wealth to the economic system to increase the
production is only useful as an intermediate step
towards increasing still further the consumption.

The most profound students of the subject have
been the most acutely aware, that, indispensable as
money is, it brings with it insidious dangers to the
very life of societies which may, as now, end in
defeating the object of its existence. It is still, as
it has always been, the Achilles heel of civilisation,
ancient and modern alike. The century that has
come and gone has witnessed a practically complete
reversal in the nature of the monetary system in
this country, from a public system with money
issued by the supreme authority of the realm to
make possible the distribution and exchange of
wealth, to a private system with money, or its
complete equivalent, issued by private people and
created by them to lend at interest.

These innovations grew up *sub rosa* and without
any definite national sanction, and it is only since
the War that it has been impossible any longer to
disguise their real character or to be blind to the
open menace they throw down to all duly constituted
law and authority. The principles that govern the
present monetary system of this country are diametri-
cally opposed to the ordinary principle of justice
and equity to which every public monetary system
has conformed. They are different from the principles
of bad or counterfeit money, as commonly under-
stood, only in the enormous extent and capricious-
ness of the money privately uttered.

.. page 20 in 2

To-day over 97 % of the total money owned by
the individuals of the nation is privately issued,
and by far the larger part of it has no tangible exis-
tence whatever. It represents a debt of goods owed
to the individuals who own it, by the nation,
enforceable by the law, which has, without the sanc-
tion of any national authority, been quietly added
to the burdens of the nation by methods that resemble
the tricks of the conjuror. During inflation, as
occurred at the end of the War, hundreds of millions
of pounds are, by these methods, uttered at the direct
expense of the other owners of money, to anybody
giving evidence of an ability to repay, and willing
to pay interest on the pretended loan. During
deflation, as now, the arteries of the nation are
sucked of their life-blood by the deliberate attempt
to destroy equally large aggregates of money. In
light of present knowledge and experience the
system appears as high treason against the nation,
a monstrous cancer invading its heart and turning
to evil the good that might reasonably have been
expected to follow the solution of the problem of
wealth production.

.. page 21 in 11

This, in the author's analysis, is the immediate
cause of the economic deadlock, but, as it is artificial,
unnecessary and anti-social, an imposition on the
credulity of the community, it is remediable. There
are more permanent and deep-seated factors than
the monetary system. The error which interprets
the whole age is the ignorance of the real nature of
wealth and the ruling passion to treat it as some-
thing to lend at interest rather than to use and
consume. This is the real antagonism between the
conception of universal wealth and relative wealth,
between the wealth of nations and of individuals,
which opposes and may prevent the peaceful triumph
of the scientific civilisation. We are here up against
the oldest unsolved problem in history, the curse
of riches, and the new economics make it trans-
parently clear. But human nature being what it
is, it is doubtful if it can be solved before the next
and Greater War ends the scientific civilisation.

.. page 22 in 22

It is not merely the paradox of to-day that poverty
accompanies wealth, as Ruskin puts it, as the north
pole of a magnet accompanies the south. Marx
has collected a number of striking quotations from
writers of the eighteenth and early nineteenth
century, well worth study.[*]_ Ortes, the Venetian monk
of the eighteenth century, who "found in the fatal
destiny which makes misery eternal the *raison d'être*
of Christian charity, celibacy, monasteries and holy
houses," said, "The great riches of a small number
are always accompanied by the absolute privation
of the necessaries of life for many others. The
wealth of a nation corresponds with its population
and its misery with its wealth. Diligence in some
compels idleness in others. The poor and the idle
are a necessary consequence of the rich and active."
Destutt de Tracy said the same in fewer words "In
poor nations the people are comfortable, in rich
nations they are generally poor." Others who are
quoted, considered poverty as necessary to ensure
the doing of the hardest and most brutalising tasks
so as to free others from drudgery, and, conversely,
that wealth unfits men for labour. Sismondi poig-
nantly stated this aspect - "Thanks to the advance
of industry and science every labourer can produce
every day much more than his consumption requires.
But at the same time, whilst his labour produces
wealth, that wealth would, were he called upon
to consume it himself, make him less fit for labour.
Exertion to-day is separated from its recompense;
it is not the same man that first works and then
reposes; but it is because the one works that the
other rests."

.. [*] *Capital*, Karl Marx, Vol. I, p. 662-664, Wm. Glaisher Ltd.,
       1920.

Grant, for the sake of argument, the problem of
the distribution of wealth to be solved as completely,
by a scientific money system, as the problem of
production, so that there is no want of a sufficient
abundance of the physical necessities and amenities
of existence. Such a community would be wealthy
in the true sense of the word. But none would be rich
in the relative sense, which is the only one in which
people use the term to-day. The work of the world
must be done as a public and professional service.
For no one would be able to take advantage of the
misery of the poor to command their labour and
service, in their own aggrandisement. This, as
Ruskin pointed out, is a consequence as much of
the poverty of the many as of the riches of the
few. To him, as to the rest, it was wealth, but, to the
new economics, it is not wealth but debt.

.. page 23 in 4

Between the wealth of nations and that of indi-
viduals there is the clearest of antagonism. An
age of universal plenty, therefore, foreshadows an
entirely new type of civilisation, which cannot
co-exist with the old. Instead of civilisation being
compelled to maintain itself and progress in the
teeth of the law of scarcity, poverty and scarcity
are now being prolonged to bolster up the original
type of civilisation. The whip and spur, the bit
and reins are being used to drive what, from being
a stage coach has evolved into a motor car. If
it is to escape disaster, the driving controls as well
as the motive power will have to be completely
changed.

Now it is one thing for science to make some
relatively much richer than others, and quite another,
without even a by-your-leave, for science so insid-
iously to undermine the established order of human
society as to put all beyond the persuasive influence
of want. There are many neither unimportant nor
over-scrupulous people, if not the majority of the
most forceful and successful people in the community,
who would probably quite openly side with no
civilisation at all rather than a, to them, so thoroughly
uninteresting and objectionable one. Some have
in fact already scented the danger. It used to be
only the genuine artists and æsthetes who railed,
quite ineffectively, at the growing mechanisation of
the age. But when the tide turns, and science
by making the poor richer makes the rich relatively
poorer, the movement to break up the machines
and revert to hand and serf labour is likely to receive
some very unexpected and effective recruits.

.. page 24 in 3

But there is no turning back. Unless the majority
are to starve, the world has to make a success of the
civilisation to which it is committed. Distance lends
enchantment to the past. The present is ugly enough,
but possibly not quite so ugly as the real past which
it tries to imitate. The future holds out hopes, of
which, till now, only visionaries and poets have
been permitted to dream, if it can escape from the
pattern of the past and begin to build anew. The
first step to that end it a scientific monetary system.
But there remains to be developed a true science of
national and international economy on which to
educate the world for wealth.

Less and less does actual ownership of possessions
constitute riches. More and more is individual
wealth derived not from owning but from lending
or having lent. The wealthy of to-day are not
wealthy in the fashion of the ancient pastoral
patriarchs, the medieval feudal barons, or even the
old landed aristocracy, in that they do not own
wealth but debts. There it a real conflict here
between the laws of Nature and the physical
necessities, apart from the avarice and ambitions of
men, better perhaps recognised by the Hebrew
prophets than by the modern economist.

.. page 25 in 10

Wealth perishes, but debts, being legal claims to
future wealth, appear to afford a means of dodging
Nature. An individual could not even amass or
accumulate the wealth required for the maintenance
of his old age, or to start his family in life. For it
would rot. He is practically forced to find a means
of getting repayment at a later date. He has to
let others spend his savings, in the hope that he
may later share in the harvest of what they have
sown. But there is, as people are beginning to find
out, a very definite limit to the possible extent to
which this can be done.

It may comfort him to try and think that his wealth
still exists somewhere, in some form, as "capital",
but as a matter of fact it was consumed by those
to whom it was lent and who produced the capital,
and wealth, no more than food or fuel, can really
be consumed twice.

The capital exists and may continue to exist a
long time, according to the rate at which it depre-
ciates or becomes obsolete, but rarely only would
the creditor take it back in settlement of his debt,
for the sufficient reason that it would in general
be quite useless to him. He must have fresh wealth,
not the product of consumed wealth, in order to
live, and however hungry, no man can eat a
plough.

Actual ownership and possession of wealth to
private people, beyond a certain maximum limit,
is an encumbrance and source of expense and
anxiety, and is only rarely a means of livelihood.
What they desire is not wealth, but debts that do
not rot, that are not expensive to keep up and which
bring in perennial interest. Individual wealth more
and more tends to take the character of legal instru-
ments and agreements - such as money, national
debt, loans to and investments in industry, - which
determine the distribution of the national revenue
as among individuals. In studying the wealth of
nations as distinct from that of individuals, it is
necessary to start at the beginning and not at the
end.

.. page 26

The original wealth it represented may have been
totally destroyed at the time the debt was contracted,
as is the case with the national debt; it may never
have existed at all, as is the case with paper and
non-existent money, or, as is the case with industrial
capital, its consumption may have left behind some-
thing of purely intermediary use in the creation
of new wealth, but almost or quite useless if the
creation it renders possible should cease, either by
its wearing out, becoming obsolete, or by reason
of people not wanting or being able to buy the
product.

The older forms of property, such as land and
buildings, the ownership of which constitutes the
chief form of individual wealth in an agricultural as
distinct from an industrialised community, are by
comparison, more generally and permanently useful
products of the wealth expended in their reclamation
and construction. But even these are now less and
less farmed by their owners and are more and more
let or lent to tenants for rent. They furnish curious
instances of the confusions of this age of transition.

.. page 27 in 10

People who own the houses they occupy, for
example, are regarded by the law as lending or letting
them to themselves, and are required to pay income
tax on the rent they are supposed to receive, the
same as if they had let them to a tenant. With
the growth of the National Debt and the spread of the
hire-purchase system, - both, by the way, admirable
instances of the result of the ruling passion to
convert wealth into debt in order to derive an income
from it - we may confidently look forward in the
near future to the same logic being applied by the
Chancellors of the Exchequer to the furniture as is
now applied to the house, and to the owner of a
house being taxed on its rental let furnished instead
of unfurnished.

Before, however, they are reduced to such lengths
in balancing their swollen budgets, it is to be hoped
that they may be enabled, by the help of the studies
in national rather than individual economy, which
form the subject of this book, to do something more
substantial in the other direction. As shown in a
later chapter, a hundred millions a year could be
immediately knocked off the national taxation as
a start, by the correction of a slight error in accoun-
tancy, natural perhaps in the general confusion of
these transitional times, but hopefully for the future
remediable at any time.

.. page 28 in 15

So that it comes about that by far the great part
of what the modern world regards as wealth, and
what is a perennial source of wealth to individuals,
is not wealth but a consequence of lending or having
lent wealth, and is in fact a form of national or
communal debt. The intense competition for foreign
or overseas markets in time of peace, aggravated
by the home market drying up through the loss of
purchasing power of the unemployed, is not due to
any altruistic or missionary spirit of the industrialised
nations in wishing to unload of their abundance
for the enrichment of the newer and less indus-
trialised nations, but to the necessity of finding new
debtors, with good future prospects of being able to
pay interest, to whom to lend their wealth. The
prospects and capacity of those at home in this
respect diminish steadily as the days go by.

The modern wars that break out between indus-
trialised nations have a precisely parallel explanation.
Then the belligerent nations rather than individuals
shoulder the debt. The glut of wealth, that in time
of peace cannot be profitably exchanged, is now
owed for as it is produced by the nations as such.
Along with the flower of the country's manhood,
it is destroyed as rapidly as the most powerful
modern engines of destruction allow. The dead do
not return, but the wealth destroyed discards its
corruptible body to take on an incorruptible. It
is national debt, better than wealth to individuals,
a permanent source of wealth, defying the passage
of time and the ravages of rats and worms. It
needs no very profound analysis to reach the con-
clusion that the most significant economic consequence
of modern war, the increase in the national debt,
explains its cause. Affluence has but turned men
into misers who will lend but will not spend, and
the most humiliating spectacle of the age is that its
best minds are devoted not to the building up of a
nobler civilisation, but to a chimera, how to convert
the wealth that perishes into debts that endure and
bear interest.
