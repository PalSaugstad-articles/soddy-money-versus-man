.. page 107

IX. DEMOCRACY
-------------

THERE is a growing body of opinion that Democracy,
in this country at any rate, has not been given a
square deal. Its political power has been useless
without real economic power. The view taken in
this book is that its fatal mistake was first in allowing
a private monetary system to grow up and then in
not putting an end to it. It is finding itself under
vague international compulsions to pursue policies
which inflict irreparable damage to its internal
economy, continuously to restrict production and
employment, to get deeper and deeper into debt,
and unable to use its strength or skill for its own
life. After over a century's unparalleled advance
in the arts of producing wealth, living is becoming
for an ever-increasing proportion more difficult and
insecure. Everyone knows that there is something
fundamentally wrong, and that the solution of the
problem is not yet within the horizon of party
politics.

.. page 108 in 5

The alarming increase of unemployment and the
continued deep depression of our staple industries
is the continuous theme of all parties, but on the
money policy as the obvious, and indeed the definitely
predicted cause, there is a conspiracy of silence.
Parliament endorses and encourages the banker in
his belief that the nation's money it his sole concern
to create or destroy as he decides. It deferred, or
appeared to defer, to public opinion to the extent
of appointing another Commission, since the notorious
Cunliffe Currency Committee that advised deflation,
to enquire into the financial system. The finding
of this, if ever published, no ordinarily intelligent
person probably would even take the trouble to
read, any more than they would of a Temperance
Commission composed of brewers. It consisted of
bankers and such experts and economists as hold
views about money acceptable to the bankers who
have taught them.

The public knows perfectly well that hardly any
step in knowledge or advance in thought, however
commonplace to-day, has ever been made without
those deeming themselves authorities in the matter
being hostile and opposed to it when first made.
To regard money as made for man rather than man
as made for money would, to the money expert to-day,
be as great a heresy as it was at one time to believe
and teach that the earth went round the sun and not
the sun round the earth. But if Galileo and Coper-
nicus had lived to-day, and had upset the theories
of the authorities regarding the nature of money
rather than of the universe, they would have had far
more difficulty in getting their new views impartially
discussed than they had from the Medieval School-
men and the Courts of the Inquisition.

.. page 109 in 2

Freedom of thought and discussion applies, as yet,
only to the affairs of the mind and conscience which
affect directly no man's pocket. It does not yet
apply to money. That is the Ark of the Covenant,
the Holy of Holies of the Slave Civilisation. Those in
authority know well the danger. It might have
consequences to humanity graver and more funda-
mental than freedom of belief. It might lead to
economic freedom, the tap-root of all freedom,
worth the name.

Look at it both ways. Who are the supermen to
govern such a kingdom, without the fear of hunger
to get its work done, the compelling power of misery
to bring men and women to its most disgusting and
and degrading offices? Though all may wish to
be rich, neither the rich nor the powerful wish all
to be rich. There is that domestic servant difficulty
and the necessity of obtaining recruits for the army
which, in 1921, made deflation appear to be the
lesser evil. So Democracy is betrayed.

.. page 110 in 14

But each of us contributes his pet King Charles's
Head. As, at the creation, the world was made with
only a limited quantity of gold, it is clear, that how-
ever rich we might be in goods, money must always
be scarce and difficult to come by. Not to have at
least a backing of gold would threaten the whole
precarious structure built upon "credit". What is
the use of giving the masses larger money incomes
than is necessary for them to exist upon and rear
the next generation of labourers? They would only
squander it to their own and other people's hurt
and become too lazy and independent to work.
Then there is the population question. Already the
world is being increasingly recruited from the
"lowest strata" of the population, and the
well-to-do are becoming less and less prolific, so that we
shall end by - but it is difficult even for the
eugenist to say how we should end if that were so.
The answer to all this is that even greater changes
are already knocking at the door. Science is revolu-
tionary, though scientific men are not, nor are
revolutionaries scientific.

To the overwhelming majority of people, democ-
racy, if it can put its house in order in time, will
appear as far preferable to any other form of govern-
ment, whether an autocracy, aristocracy, plutoc-
racy, military dictatorship, or any combination of
these. It is premature to condemn a system of
government which is not the real government. The
most important change that it is possible for a
nation to suffer, that its monetary system should
be surrendered to and become the monopoly of the
money-lender, has taken place in this country with-
out the public being aware of it, and without the
matter being a political issue at all.

.. page 111 in 11

The way lies open for a real democracy with
opportunities for all to live an ample and civilised
life. Judging from the rapidity with which those
whom science has already enriched have managed
to efface the sins and shortcomings which they con-
demn in those less fortunate than themselves, possibly
the eugenist is mistaken in his insistence on the
influence of heredity and his neglect of the importance
of economic environment on human character and
worthiness. Unless the wealth of the world is used
in the attempt, at least, to elevate the general level
of humanity to something of the standards of those
whom it has already elevated beyond the fear of
want, there can be no serious question that it will
be used to destroy it. If we are doomed to fail
either way, the first way seems the more rational
and inspiring. If the world cannot be made safe
for Democracy it seems impossible that it can be
made safe at all. Dictatorships and autocratic rule
offer no final solution of the real problem, the wise
consumption of wealth.

The betrayal of Democracy was originally due
to ignorance. To-day it would probably be truer to
say that fear and distrust of the people are responsible
for the real economic strangle-hold of money being
left in private hands. Parliament would not dare
openly to do the deadly work that has been going
on since the War. It shelters itself behind the plea
of impotence. "The grim goddess of Finance
exercises, as she always must, an inexorable
power."

.. page 112 in 12

So that is all it has been for! In place of the old
grim reality which science has abolished, the poli-
ticans have been successful to the extent of creating
a new grim deity - a counting goddess who, by
counting below the level where there is anything to
count, can hit recalcitrant humanity below the belt
in the comforting assurance that there will be little
there to hit. This algebraic paragon is supreme in
understanding the profound social significance of
the transcendental truth that minus one is plus one,
compared with minus two. It can make even sane
men believe that poverty and despair are the normal
and natural consequences of over-production and
superabundance.

This is no exaggeration, but the authoritative view.
Power and responsibility seem to dull men's wits.
No less a person than the late Premier asked the
celebrated question "What is the use of making goods
if we cannot sell them?" when an ordinary man
would have asked "Why cannot we sell them?
What is money for?" To the ordinary man,
if a business is incapable of doing its job it
is the obvious course to replace it by one that
can. But to the man in control, when money fails
to do its job, there's an end to it. The whole financial
system of this country is so rotten that it cannot face
a genuine enquiry. At present it is costing the nation
over a hundred million pounds a year in bogus
taxation for not doing its job, the distribution of
the goods the country can make and does need.

As the "foreman" type might well scorn any
lesser weapon than brute violence and misery to
get the roughest jobs put through, so the democratic
ruler has to have something behind him more
effective than the silvery tongue of oratory when it
comes to getting the world's work done. It is all so
natural. Yet some of the hardest and, at the same
time, most disgusting work of the community, - that
of hospital nurses, as one example, - is done volun-
tarily at a necessary and benevolent public duty
with but little financial inducement. If it did
nothing else that was good surely the Great War
established that the spirit of Democracy deserves
better than this of its ostensible rulers.

.. page 113

This age of misers and misery has, perforce, had to
divorce youth from political power for the century
past. The inverted top-heavy condition of a world,
engaged in the amassing of debt, cannot safely be
entrusted to the young and rash. It requires experi-
ence, wisdom, compromise, diplomacy and very
skilful balancing. The young readily reorientate
their minds to new conditions. The old, at best,
apply forty years too late those they had arrived at
in their youth, and these, more often than not, are
mere pitiful efforts to mould the hated new into the
pattern of the beloved old, rather than anything
adequate to and worthy of the age. It has been
wittily said by Professor de Madariaga, that so long
as there is death there is hope. But it is clearly
asking impossibilities of the world that it should stand
still and give its leaders time to catch up with it.

.. page 114 in 15

So far as contemporaneous politics have any
reality or applicability to the mode of life of the
modern world, it can only be to the mode of dis-
tributing wealth. The politicians, like the economists,
lawyers and bankers, are usually tyros in the science
of creating wealth. It would seem that there are
broadly two possible methods, by no means mutually
exclusive, by which the revenue available for the
maintenance of life may be distributed. We may
follow our present tendency of trying to make dis-
tribution, like production, communal, of providing
State services for education, old-age, sickness, housing,
etc., out of taxation, in addition to the necessarily
communal services, until taxation becomes the
regular method by which the revenue is distributed.
We so, at most, provide some people out of other
people's pockets, not with what they want them-
selves, but with what the good government is graci-
ously pleased to think necessary for them, much
as a slave-owner looks after his slaves, or a farmer
his cattle, and we expect individuals, as before, to
continue to supply all the capital.

No one seriously defends such a method, except
as a mere temporary alleviation or evils none of
which are necessary or natural, least or all in an
age which has seen the solution or the problem or
production. Alternatively, we may follow the
methods, outlined in the preceding chapter, or
distributing larger money incomes, leaving the
individual free to choose what he thinks best.

The continuous struggle to maintain the decencies
or existence against the forces tending to enslave
and submerge them since the beginning of the
industrial era, has developed among the rank and
file of the industrial army a team-spirit unknown as
yet among the well-to-do as a class and with obvious
resemblances to that in the professions or law and
medicine. They also, if not their vocal champions,
naturally have more actual realisation of the nature
of the processes by which wealth comes into existence,
and what it costs in human-being-hours to produce,
than the mercantile, financial and professional
elements of the community to whom such considera-
tions are but figures and symbols.

They should not long remain under the general
misconception that the expropriation or the incomes
of the well-to-do to provide their needs is socialistic.

.. page 115

It is enslaving and demoralising rather than
liberating them, and, at most, shifts the ownership
of capital from one individual to a number of often
less-desirable masters. Practical Socialism, as the
national ownership of the means of production,
distribution and exchange, requires national or
government saving or thrift rather than government
expenditure on the maintenance of individuals.
All that is being spent on the alleviation of poverty
and distress is taking them further from rather than
nearer to their goal. So far, Labour in power has
but followed the path of least resistance leading
nowhere. By expropriatory taxation, it obtains and
spends as revenue what in a Socialistic Common-
wealth it would have to spend on capital. In this
of course it is opposed by the older parties who, in
power, do the same to even greater extent, because
unopposed.

So it is with the restriction of output by the Trade
Unions, the attempt to make a day's work last a
week, the putting of sand, instead of oil, into all the
wheels of the industrial system. They are taking
us further from Socialism, and the goal to economic
freedom lies in an opposite direction. These are the
roads to national impoverishment and demoralisa-
tion, and Socialism is no more possible to an impover-
ished and demoralised nation than it was in the
age of scarcity.

.. page 116 in 4

If it is to come in our time, not only the money
barrier, but every other barrier to full and efficient
production must be swept aside, so that there may
be at once an ample revenue and sufficient for the
provision of new as well as for the redemption or
the old capital. To such an age, freedom might
be something more than a wish or a name to the
many. The standard, of professional service, the
morale of the officers and men of a great army, the
solidarity and loyalty of labour, developed in the
hour of its trial, must be the principles the world
must rely upon, if it is ever to give up its belief in
the efficacy of want, fear and greed. Education for
wealth is the only hope left for the race.

Yet it is idle to attempt to appear even optimistic.
The old regime has left behind its heritage of deep
demoralisation and revolt. The present generation
of labour are preparing for Armageddon when they
should be educating themselves for the Promised
Land. As though the forces making for destruction
were not already powerful enough, they increasingly
tend to be wreckers and look to no other way out but
a revolution. No doubt this will appear monstrous
and treasonable to those to whom the inexorable
laws of economics have been kind, but to the pro-
fessional economist it must appear as the most
beautiful and convincing demonstration of the
fundamental principles upon which he has founded
his subject, in directions at first overlooked.

.. page 117 in 7

The greatest obstacle to all reform at present comes
not from the "True-Blues" and "Die-Hards"
but from the natural drift of Labour towards revolu-
tion. Those most vocal on the subject of the wrongs
of the poor and exploited have vested interests in
their continuance and growth. "Monetary reform
will rivet the chains of wage-slavery and capitalsm
on the workers for another hundred years." Some-
thing like this would now be effectively said of every
real, as distinct from sham, reform. They seek to
end rather than mend the system, and in this they
are likely, as has been indicated, to find recruits
in very unexpected and powerful quarters.

Meanwhile those who have believed in the people
and the ultimate triumph of the democratic idea have
small cause for hope. There is little evidence of any
enthusiasm for or interest in what the future may
have in store. No one would conclude that, since the
last war, the nations have been engaged in a race
against time. Rather Democracy seems stunned,
pole-axed by the last blow and awaiting the next,
disillusioned, cynical and frankly infidel, but know-
ing no other way.

The march of events has put the world out of its
focus. The froth of wordy battle and the clap-trap
of the hustings, no more than the issue of the last
great conflict, now decide anything at all. Heroes
and their worshippers occupy the stage as fancy-
dress mummers detected by the cold eye of dawn.
The issues have been shifted from the personal and
emotional spheres in which the qualities of men
have evolved to that of the intellect and under-
standing. Ideas are at war rather than nations
and men.

.. page 118 in 5

Clear thinking, independence of judgment, sanity,
ability to grow up mentally and take a road that is
strange and new to history, in the teeth of all prece-
dent and authority, are the qualities of the pioneer
rather than the many, and now they are being
demanded of the many. Will Democracy stand the
new test? The dangers that crowd in upon it come
from behind, but straight ahead the way still lies
open. Can it steer the straight course? Nowadays
the individual may, at most, be a lone signpost to
the many. They must blaze their own trail.

FINIS
