# Frederick Soddy's book MONEY versus MAN

This is a 'reconstruction' of the book 'MONEY versus MAN'
by Frederick Soddy. The book was published in 1931.

There are some scanned versions of this book on Internet:

https://www.flickr.com/photos/bn37ej/3435808100/in/album-72157616673992980/lightbox/

https://dspace.gipe.ac.in/xmlui/handle/10973/22230

I think the latter used the photos from the first, but applied perpective correction on them.
The latter also includes an OCR text version of the page images.

In this repo I try to manually correct the errors produced by the OCR process.
