.. page 90

VIII. PRACTICAL MEASURES
------------------------

IF THE principles expounded in the preceding chap-
ters were properly understood, the actual application
of them to meet the existing conditions of the world
should not prove a difficult task to competent legis-
lators and administrators. For this the writer has,
of course, no brief, but it seems advisable to give
a short account of the sort of measures that appear
to him advisable, more in illustration of the principles
than as the best or only ones that would serve. It
is inevitable that the British nation, with its love
of compromise and inability to learn except by
actual adversity and costly experience, is destined
to try many devious roads before being forced to
take the direct one. Whereas, to anyone with any
training in physical realities, the direct one is the
only one worth taking.

.. page 91 in 8

In the first place we have to make a total end of
the system of fictitious credit. Let us be clear of
the difference between the false and the real. A
real credit is put through by someone selling some-
thing of value. This does not affect the quantity
of money, for the seller obtains money, which the
buyer gives up. When the former expends the
proceeds on the goods necessary, and in paying the
wages and salaries of those engaged in production,
he is doing what, by his own action in obtaining
their money, he has prevented the original owners
of the money from doing instead of him. He gets
what he wants, and other people receive his property
in exchange.

Now consider the existing system in periods of
inflation, as occurred during and after the War, and,
in periods of deflation, as the present. In the first
place new "credits" are being granted faster than
those granted are being repaid. Anyone with any
acceptable security, easy to sell and not likely
suddenly to depreciate, - and for this purpose a gilt-
edged investment, such as some National Debt
security is preferred, - by depositing it at a bank
and paying the current rate of interest, is accommo-
dated with a "loan" or "advance" for some sum
short of the nominal value of the security. Nobody
willingly gives up anything at all, but the person
accommodated is empowered by the bank to obtain
other peoples' property up to the extent of the loan,
and the money they receive in exchange is an addi-
tion to the quantity of money in existence.

Let us suppose that the security is some form of
the National Debt. Interest is still paid by the
nation to the owner of the security, and is passed
on by him to the bank with whom it is deposited,
in whole or in part to meet the charge of the bank
for interest upon the "loan". Hence a highly
curious and instructive situation arises.

.. page 92 in 2

The new money issued by the bank would have
had exactly the same effect on prices, whoever had
issued it. The State, when it borrowed in the first
instance from the owner of the security, was acting
in justice to the owners of money in general, so that
its debt to them should not be repudiated in part,
as it would have been if the State, instead of borrow-
ing it, had itself issued the money. This it precisely
what the banks have now done, and we reach the
amazing conclusion that the State is now paying
interest on the loan to the banks for doing the very
injury to the owners of money for which interest
is being still paid to prevent being done. The
reason why the subject of money and high finance
is such an unintelligible one it because it consists
of transactions such as this on a gigantic scale, for
which outside of a lunatic asylum it would be difficult
in the twentieth century to find any analogy or
parallel.

When the increase of the currency by such means
has gone too far so that prices rise sufficiently, all
economic relations are upset. There it strife between
employer and employed and a general demand for
higher wages and salaries owing to the high cost
of living. There is an accumulation of new goods
coming on the market, and increasing competition
for sale, because, owing to the rise of prices
the money can distribute no more goods than
at first. Money has now the whip hand and can
dictate its terms. Regardless of the consequences,
deflation now takes the place of inflation. Exist-
ing credits are cancelled faster than new ones are
issued, so that the total amount of money is
reduced.

.. page 93

This attempt to reduce prices and wages is at first
stubbornly and actively resisted. The production
of wealth is slowed down, and less sells at the enhanced
prices, rather than the same amount at lower prices.
Poverty is now produced by glut not by scarcity.
More and more of the personnel of industry are
turned off, until economic distress and insecurity
of livelihood compels the recalcitrant workers to
accept lower wages. The nett result is to leave the
nation impoverished and its internal relationships
exacerbated, by virtue of the great business losses
incurred, which are on a par with those that would
occur if weights and measures could be fraudulently
manipulated, and of the strife between employers
and employed.

Modern money is a form of internal debt enforce-
able against the individuals of the community, and
not at all against the persons who issue it.

.. page 94 in 10

The Government responsible to a Democracy has
no more right to allow private people or firms to
appropriate the proceeds of the issue of money
than the proceeds of a State loan.[*]_ By doing so
they have betrayed their trust. The loss of the
unclaimed wealth to the nation is the least and
most insignificant of the evils that follow from allowing
a private money system. Before the day when money
rose to its present power, in the early days of demo-
cratic Parliaments, such a situation as at present
exists would have had the support and defence of
no party. It would have been the target of righteous
scorn and ridicule of every individual representative
of the people.

.. [*] It would appear from Section 6. subsection (1) of the 1928
       Act this principle has been recognised so far at least as concerns
       the retired Treasury Notes, but the language is capable of a
       number of meanings. The point stressed here is that it should
       apply as a matter of course to all money.

The mistake in those days was, indeed, the same
as now. Parliament has always been afraid to issue
money sufficient for the nation's needs, as gold and
silver and the older methods of distributing the
revenue became insufficient for modern production.
It was afraid of being deemed immoral and fraudu-
lent by the ignorant, if it got "the something for
nothing" which it is impossible not to get by the
issue of modern money, and if it dared to pay its
way in part by this method, rather than by the
"honest" method of imposing taxation. But, in
those days, it was equally particular that no private
bank or firm should do what it considered would
be regarded by the public as immoral and fraudulent.
Then the cheque system was invented, which
relieved the impossible situation by allowing banks,
without the public knowing it, to issue the money
the Parliament dare not itself do openly. Now
the interests in this practice are so gigantic that
they can suppress, to a large extent, any public
discussion of the subject that is unfavourable to
them.

.. page 95 in 6

The replacement by national money of the two
thousand or more million pounds sterling issued by
the banks, is an act of tardy justisce to the com-
munity, and, if this generation has not the courage
to put it through, we may rest assured a more en-
lightened future generation will. Prudence suggests
the sooner the better, for the longer it is delayed,
the less there is likely to be to show for the money.
It needs a Chancellor of the Exchequer, failing a
special Minister, who will exercise his normal function
as the trustee of the nation's finances.

At the present time over £100,000,000 a year is
extracted from the taxpayer as interest on the
National Debt, and is being paid *via* the owner of
the security to the banks to which it has been handed
over as cover, as payment to the banks for doing
what the interest is the consideration for not doing.
The State borrowed the capital sum and pays
interest upon it, in the interests of common justice
to other holders of money, so that their money shall
not be depreciated in value, as it would have been
if the State, instead of borrowing, had simply issued
the money by the printing press. But the capital
sum, though not printed, has been issued by the
banks as bank "credit" with consequences exactly
the same as if the State itself had issued the money.
So that the taxpayers are paying annually over
£100,000,000 a year for no consideration whatever,
and this absurdity should first be cleaned up.

.. page 96 in 9

The situation is that some £2,000,000,000 more
money than exists is at present owned by people
with current accounts. Suppose, in the first place,
this money is issued by the Nation as a loan to the
banks for a short transition period, and they are
legally compelled henceforth to keep £ for £ of
national money against their liabilities to current
account holders. The newly issued money is thus
immobilised in the banks who for the first time in
their history are solvent against even the most
sudden and violent financial panic. Naturally the
banks will not now continue to keep current accounts
for nothing, and they are in a position to repay anyone
who does not wish to pay their terms for the service.

The situation then is that the banks owe the nation
£2,000,000,000 and are owed this sum by industry
and other debtors at call or for short periods. As
these "loans", mature, they are not renewed but
repaid to the National Exchequer, and immediately
put back into circulation by the buying of equivalent
national securities for cancellation. Henceforth all
bank loans must be genuine, if the £ for £ provision
is made effective.

As regards industry, short term loans are as dis-
astrous to the community as pulling up a tree by its
roots every now and again, to see they are all right,
would be to the tree. There is no justification for
them if money is of unchanging value and the
national needs are to be catered for smoothly and
uninterruptedly. Industry can be neither created
nor expanded without irrecoverable expenditure in
the sense already explained, which must be provided
for by genuine credit or out of taxation. Every
business, except banking, has to have sufficient real
capital and it is not provided by creating debts in
ledgers.

.. page 97 in 5

As for the banks, their legitimate business is to
lend not create money. Painful to them as the author's
criticism must neccessarily be, it is possible they
may in some future age put up a tablet to his memory
for helping to deliver them from their Sisyphean
task. For their legitimate business, as indeed all
legitimate business, cannot help being very greatly
expanded if the strangle-hold of money over men
were removed. This, on the other hand, will
probably antagonise the Labour Party to the whole
proposal, for the thought of anyone making extra
profit is painful to them. As, however, science
literally threatens to destroy the world if it insists
on remaining poor, their objections must be waived.
For conscientious objectors to an age of plenty the
monasteries would still be open.

With regard to the whole question of lending for
investment, there is room for reforms much overdue.
Whatever may have been once the case, genuine
industry should now not have any difficulty in
securing all the capital it may require. Vast sums
in fact are now wasted for lack of proper guidance
and protection in this respect. According to Mr.
Henry Morgan, President of the Society of Incor-
porated Accountants and Auditors, in the last six
months of 1928 the public invested £15,117,000 in
58 new companies. The estimated profits of 52
of them were given as £5,219,000 a year. Now,
two years after, he estimates that the public had
lost 95% of their money. Only one of the 58 com-
panies had so far paid any dividend, 27 were already
in liquidation (14 of them under compulsory orders),
and, of the remainder, at least 16 were in serious
difficulties.[*]_

.. [*] See Report in *The Times*, 26/9/30.

.. page 98 in 2

Surely it is time something was done, and this direc-
tion of the flow of investment should be legitimate
banker's work in which they might very much
extend their usefulness. If they are unable to, the
needs of industry can easily be met by the Govern-
ment securing the rights and interests of investors
who wish to invest their savings under their
protection. One would have thought this to be
one of the natural functions of the law in any
case!

As for the future provision of such additions to
the total national money as will keep the index
number constant, as production and population
expand, it would seem that what is required is a
Bureau of Statisticians analogous to the National
Physical Laboratory, which standardises our weights
and measures, to determine index numbers and advise
Parliament as to the rate the new issues should be
made. These issues would be made either as the
original issue, by retiring State loans, or put into
circulation in payment of part of the national expen-
diture that would otherwise be defrayed by taxation.
Some people seem to have an exaggerated idea of
the importance of giving the money direct to
consumers, as would be the effect of the latter
alternative. The point of first importance is that
all should benefit, as all, in greater or less degree,
unwittingly contribute to the new issue, but there
may be secondary considerations dictating which
method may be adopted. The question is the same
as would arise under the second part of the scheme
still to be dealt with, whether to use the national
revenue for the current expenditure or to redeem past
capital indebtedness.

.. page 99

It is always quite gratuitously assumed, by the
defenders of our devious methods of creating money,
that Parliament is as a matter of course not to be
trusted, and that the temptation to meet expenditure
by issuing new money instead of by taxation, in
peace-time as well as in war-time, would be irresist-
ible. Even if this were true, it should not be difficult
to guard against it. The Bureau of Statisticians,
instead of being merely advisory, might be given a
status similar to that of the judicature in the adminis-
tration of Parliamentary enactments, so that their
decisions were as binding upon Parliament as any
other legal decisions are, unless set aside by special
legislation. But, if it were laid down that all the
profits of the issue of money should go automatically
to national debt redemption, in the same way as
any surplus of the revenue over expenditure now
does, not only would a step be taken which would
protect this source of revenue from the rapacity of
future Chancellors of the Exchequer, but a welcome
and very necessary new provision for the cancellation
of this debt would be made.

.. page 100 in 10

This seems to be the first step the nation will have
to take along the road, the goal of which is economic
freedom. It may be only a small one, but it is the
first step that counts. It would ensure that the
regular production and consumption of wealth
should increase up to the natural limits set by the
availability of capital and labour, and the industri-
ousness of the people. There would be no involuntary
unemployment, and apart from physical or mental
defectives for whom medical or educational provision
rather than indiscriminate charity is needed, there
should be no undeserved destitution.

The important distinction between the system
proposed and the present one is that, while the law
of supply and demand would operate as before as
between one kind of commodity or service and another,
there never could be general overproduction and
unsaleability for lack of money. That would be met
by greater purchasing power among consumers, so
that the standard of life would gradually rise. On
the other hand, there is nothing whatever in these
proposals to encourage waste, idleness or exorbitant
demands on the part of one act of the community,
relatively to the others. For if everyone became
demoralised by prosperity and too lazy to produce
wealth, so that prices tended to rise, the correction
would be applied by taxation withdrawing money
from circulation to prevent the increase in prices.

.. page 101 in 14

It is, of course impossible to predict for the indefi-
nite future, but, for the present, we are still very far
from any real sateity of production. There is so
much leeway to make up in under-consumption
that problems that would arise if ever real over-
production occurred may be left safely to the future.
The problem to-day is under-consumption and the
solution in general terms must precede the solution in
detail. We must act up a monetary system capable
of distributing all the wealth that can be produced
before indulging in partizan disputes as to the relative
shares of individuals or classes in the product. The
future safety of civilisation depends upon finding
an outlet for the world's potential production by
useful consumption, which shall build up and enrich
life in all its varied aspects, rather than destroy it
miserably by a succession of ever more disastrous
economic wars. A national scientific money system
would create an enormous home market for the goods
we can so plentifully produce, as well as facilitate the
barter of these goods for others more favourably
produced in other countries.

Hitherto the whole thoughts and energies of the
world have gone to the initiation of a new system.
Acceleration and the speeding up of production,
labour-saving, the reduction of working costs, and
the reduction of consumption to the minimum in
the interests of accumulation, are the views still
left as a legacy from that byegone era. No thought
has ever been given to the ultimate purpose of all
the wealth we have been patiently preparing for
over a century to be in a position to produce. Now
we can make it no one seems to know what to do
with it. Except as a purely transitory phase, the
idea of producing more and consuming less is a
contradiction in terms. If something is not speedily
done to increase consumption, the world will speed
up till it bursts. Already it might be satirised as
more remarkable for its revolutions per minute than
for its miles per hour.

.. page 102 in 6

Once the whole unemployed labour and capital
are again absorbed in industry and the production
of wealth increased to the maximum possible, *then*
is the time to take the next step, which to those who
understand the principles involved will need little
further elaboration. From the standpoint of national
economics capital expenditure, necessary and pro-
ductive as it is, is expenditure, not accumulation, and,
in so far as it is not redeemed or wiped off, remains
as national debt, - just like any other national
debt, a source of income to some members of the
community at the expense of the whole. Its owner-
ship by individuals results in a redistribution of the
revenue as payment for its hire or use. But from
the national standpoint its provision in the first
instance is an irrecoverable expenditure precedent
to any of the modern methods of production. Unless
redeemed, the debt will accumulate indefinitely and
this civilisation, it would seem, is bound to go the
way of other past civilisations that became wealthy,
through the growing power of the creditor class
over the debtor class, and the reduction of the latter
to the rank of their personal followers and servants.
Whereas economic freedom lies along the opposite
road.

.. page 103 in 13

A national monetary system would benefit the
poor equally with the rich, and would remove the
immediate cause of the worst symptoms of class
warfare in the body politic. There would result a
larger flow of money not only for individual liveli-
hood, but also a much larger total revenue to be
levied upon by the tax-gatherer, so that the income
from this source could be increased even though
the rate of taxation were reduced. There would be
more money available for investment, and that is
all to the good, as it could not but help to lower
the rate of interest. This will increasingly tend to
prevent an over-production of capital, for people
who might be tempted to "save", when the rate is
5%, would prefer to spend if the rate were reduced.
At the same time, even though there were no interest
inducement, responsible people in an individualistic
society would be compelled to try and save, as has
already been pointed out.

The next step is the redemption of old capital by
the allocation of a proportion of the revenue derived
from taxation. This is a perfectly sound and practical
scheme for the gradual nationalisation of capital,
for achieving gradually the original intention and
object of Socialism, as the national ownership of
the means of production, distribution and exchange.
It is as different from anything that passes for the
name politically to-day as vinegar is from milk. It has
a catch in it. It is effected not by oratory or by
violence but by paying for it, and if ever it is put into
operation it would probably not be by political
socialists at all but by the older parties.

.. page 104 in 13

Modern expropriatory taxation, so-called socialistic
but actually purely individualistic, merely alters the
identity of the owner of capital from A to B, C, D, ...
It exchanges one aristocratic devil for seven plebeian
ones. It does nothing whatever to vest the capital
in the nation as a whole. In future the nation
should "save" as well as individuals. Taxation
should be for two definite purposes, the provision
for government expenditure, which at present is
the only one, and the second for the purchase of
capital investments. The first under economic pros-
perity should be a diminishing and not an increasing
burden, as all the ameliorative payments to paupers,
the unemployed and for the innumerable services
which an economically free and prosperous people
would naturally prefer to pay for themselves, becomes
more and more unnecessary. If, as elsewhere worked
out,[*]_ a 4/- in the £ income tax derived from invest-
ments were earmarked for the purchase of those
investments, and the subsequent interest payments
of the investments so acquired by the nation were
devoted to the same purpose, the whole of the now-
existing capital would pass into the ownership of the
nation in about twice the time required for the
investment to return the capital in interest, that is
to say, in 40 years for a 5% investment, in 50 years
for a 4% investment and so on. Of the capital
accumulated in the interval from the initiation of
the scheme, the nation would also own a proportion
increasing with the age or the capital. The scheme
may be called one or compound redemption, the
interest on the capital acquired being used as well
as the revenue from taxsation to effect the redemption.

.. [*] *Wealth, Virtual Wealth and Dept*, p. 272.

.. page 105 in 11

Expropriation *en bloc*, as advocated by the com-
munists, would certainly be speedier, but it would
result merely in the government being the capitalist
and the communist the governed. *Plus ça change,
plus c'est la même chose*. The differences between one
political system and another are far less fundamental
than is commonly supposed. The system which is
usually called capitalism is in reality the scientific
civilisation. Capital is the essential, and it it merely
in the ownership of it, and the way in which it is
provided, that capitalism differs from socialism. If,
under the latter, the provision were neglected in
any of the highly populated industrialised nations
the majority would starve. The point seems to be
neglected by the revolutionaries that those who
produce capital are consuming but not actually
producing. They are merely preparing for future
production. The actual processes involved are
identical whether capital or consumable wealth is
being produced, but in the first case the product is
not consumable. Hence those who produce con-
sumable wealth must always produce more than they
consume, by the amount consumed by those pro-
ducing capital. Then, but not otherwise, all can
consume more than they could without such capital
provision, and the population can grow beyond
the limit of the former mode of life. The principle
of Malthus is offset.

The reforms here stressed, if applied, would result
in a system with the good features of both the individu-
alistic and socialist states. All the old capital would
automatically be acquired by the nation, by
applying the taxation on the revenue it provides to
its owners to its redemption, and the whole revenue
therefrom would ultimately be vested in the nation.
Whereas the starting and development of new
industries and sources of income would be left to
private initiative and enterprise, and in this it is
hardly to be questioned that individuals are better
than any government department could be. In
fact it is to be doubted whether it could be done at
all satisfactorily by governments as at present
elected.

.. page 106

If ever science is developed to the point that there
is less and less for the human worker to do, if more
and more of the routine of production continues to
be accomplished by automatic labour-saving devices,
so that only a fraction of the human labour and
services available are required for full production,
- which is a state of things by no means so remote
as at present it seems, - the situation will be
adequately met. For the nation will then be in
possession of a source of revenue of its own, which
is not the case now, out of which national dividends
in lieu of wages could be paid, as the Douglas school
advocate, to all, or to all who are no longer required
to carry on the economic work of the world. It will
probably come as a surprise to many people who
have not gone deeply into the question, that this
supposedly wealthy nation, and others are the same,
owing to the false system of private economics
posing as national or political economy, owns
practically nothing at all. It is in the position of a
poor relation to its citizens, and is dependent upon
what they may graciously be pleased to grant from
year to year to enable the national services to be
carried on. A rich nation to the old economics is a
poor nation to the new.
