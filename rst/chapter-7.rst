.. page 80

VII. INTERNATIONAL ECONOMICS
----------------------------

.. page 81 in 24

MOST impartial people can be got to agree that a
national monetary system on the lines indicated
might be an improvement upon the existing one,
but express the doubt whether one country could
adopt it without the others, and argue in fact that
monetary reform must be international rather than
national. If we examine this argument it will be
very difficult to find any justification for it. Every
country has its own system now; they are all different,
and, if there is to be reform, one country must lead
the way, and would stand to profit by its initiative.
After fifteen years' residence in Scotland, the author
left the country with the view that the admitted
good qualities of the Scot, generally ascribed to
their character and moral virtues, were at least as
much the result of the fact that a century ago they
were in advance of the rest of the world in their
banking system, and had devised a system of issuing
money by the banks in accordance with their needs.
Now they have come under the centralised inter-
national money power their former advantage has
become a present handicap, so that, in truth, now
on crossing the Border there is not much evidence of
it, nor of any peculiar economic freedom or indepen-
dence in Scotland, but increasingly just the reverse.
They furnish an object lesson of the thesis justifying
this subject and this book, that the economic basis is
the foundation of all national greatness.

It is true that this country is dependent upon its
foreign trade for the greater part of its food supply,
and any monetary system that interfered with this
supply would be condemned, however much it
might facilitate internal trade. But precisely in
what way an honest monetary system is going to
injure foreign trade is left to the imagination. It
is an objection used to frighten the timid. For
what other countries are concerned with about a
nation's money is not how it is issued, but what
it is worth in goods. A money invariable in
terms of goods in general is exactly the kind that
will facilitate foreign trade equally with that at
home.

What is proposed is the modern method, which the
growth of statistical economics and the use of index
figures renders possible, of doing what the old gold
standard tried to do without success. The goods
value of gold over the past century has been a
widely variable one, as Irving Fisher has been at
pains to expose, and these changes have had most
distressing social consequences.

.. page 82 in 6

The real argument here against reform is pre-
cisely the same as against any reform, that it will in
time destroy the basis of the old civilisations
founded essentially on poverty as the lot of the
majority. Extraordinarily difficult and confused as
are international economic problems in terms of the
mental outlook derived from the past age of scarcity,
they are, as a matter of fact, starkly and uncom-
promisingly clear in terms of the New Economics.
For the same general point of view that illuminates
and clarifies our domestic economic situation, is
applicable with even greater directness and definite-
ness to international affairs.

If it were merely a question of this country obtaining
all the goods it requires from abroad in exchange for
equivalent goods exported, there would be no diffi-
culty whatever. The monetary system that allowed
our industries to function at full power at constant
price level would stimulate equally the barter of
those goods for foreign products. To correct any
small balance of international trade, gold could be
used. - indeed that would be its only remaining use
as money, - and simple measures could, if necessary.
be taken to keep the gold reserve of any country
within definite limits.

The difficulty is entirely different and a much more
sinister one. It is not this sort of export trade or
barter that the political world is getting anxious
about, and in order to bolster up, is proposing to
jettison the principles of free trade and to adopt
tariff barriers against the foreigner with preferences
within the Empire. It is the sort in which our goods
surplus can be sold abroad *without* goods coming
back to pay for them, to create interest bearing
debts for the future maintenance of the wealthy
classes at home.

.. page 83 in 2

So far as the ordinary citizen is concerned, most
of what passes for foreign trade the nation would be
far better without. As is well known, the money
created so easily in this country by banks is to a great
extent immediately transferred to other countries,
producing dislocation of the foreign exchanges, for
transactions which have nothing to do with goods or
*bona fide* trade at all - it may be to take advantage of
some panic or upheaval on a foreign stock exchange.
Put a stop to any speculator, who wants money,
having it issued to him straight out of the pockets
of the real owners, to gamble on margins with,
reduce foreign transactions to genuine barter and
*bona fide* dealings in investments, and the problem of
the foreign exchanges, and, of international re-
lations in general, would be much simplified. This
is precisely what a national monetary system would
naturally do.

If what may be termed the "starting mentality"
leads to congestion and deadlock in national
economics, in international economics it is a per-
manent danger to the peace of the world. In home
affairs, the next step, after getting the economic
system to full work again, is to deal with the growing
load of capital indebtedness, and to secure for the
nation, out of revenue derived from taxation, the
capital so as to make way, as it becomes obsolescent,
for its continuous replacement by new. Applying
this to international economics we must look forward
in the future, not to a continuation of "a favourable
balance of trade" and to continuing to export
capital, but to the repayment of the enormous
total debts contracted by the newer countries to
this and other industrialised nations.

.. page 84

Sir Robert Kindersley in a recent address to the
National Savings Movement at Cambridge[*]_ gave
some interesting statistics, at which he had arrived
with regard to this country. We receive £300,000,000
a year in interest and sinking fund from foreign
investments. Prior to the War, imports (goods and
services) paid or nearly paid for exports, and the
whole, or by far the greater part, of the interest and
sinking fund received in respect of past indebtedness
went back for reinvestment abroad. He deplored
that now only one-half is going back. The result
according to this authority, must be disastrous
to our export trade "which in the past has been
built up and suistained by liberal investments in
those countries which are willing to take what we
produce".

.. [*] Reported in *The Times*, Sept. 20, 1930.

Thus this "export trade" is a very curious one,
as we not only export the goods, but we pay for them
also. The foreigner can only buy our goods if our
investors hand over their savings to him to buy them
with. The capital indebtedness of the rest or the
world to us, - the unknown total that brings in
£300,000,000 a year as interest and sinking fund,
which before the War had been growing at compound
interest, the whole of the proceeds being returned for
reinvestment, - is still growing, but, as only half is
reinvested, our "export trade" is already in danger.
What it will be when the tide turns, and the newer
countries themselves become industrialised and
wealthy, and desire to repay, no one ever contemplates.

.. page 85 in 1

A careful study of these facts will reveal to the
thinking reader more about the true character of
modern wars than anything likely to be learnt from
the apparently rather ineffective people trying
ostensibly to stop them. They start from a totally
false assumption - that modern wars are due to
human pugnacity and quarrelsomeness, that sud-
denly a nation is seized with an uncontrollable
passion that leads to it attacking and attempting to
exterminate its neighbours. Whereas everyone
knows, or should know, that it is only with the greatest
difficulty and by sedulous dissemination of untrue and
misleading propaganda, that nations can be worked
up to the pitch of declaring war upon one another.
When war is once started, it may be relied upon to
feed the flames of hatred and revenge. But, even
so, it was remarkable during the last war what a lot
of clever people, expert in national psychology,
had to be mobilised to invent and disseminate
false stories in order to keep the spirit of the bel-
ligerent nations up to fighting pitch.

What applies for one industrialised nation like
ourselves, applies equally for others. Presumably
Germany, France and the United States are also all
out for a "favourable balance of trade". The
apparently flourishing export trade of industrialised
nations during last century has been built up because
the goods are not paid for, but owed for, just as the
wonderful economic prosperity experienced by this
country during the War was due to precisely the
same reason. It is the wealthy man's one idea of
what wealth is for, to lend to those who are in need,
at interest, surviving from the age of scarcity.

.. page 86

Even the slackening of this export trade immobilises
our factories and workshops, and the turning of the
tide. - which is the natural development to be antici-
pated as the rest of the world becomes industrialised,
and adopts scientific methods, - would "deluge us
with a flood of foreign goods". So that we may
look forward under the system to being able to
employ less and less of our own workers. Indeed
we are already a long way on this road to national
suicide.[*]_ Whether the foreign debts are repaid or
continue to increase, we are heading for a position
in which a rentier class at home live mainly on the
interest or repayment of foreign investments, and
a larger and larger part of the population can only
live as their personal dependents and servants, and
less and less by genuinely productive industry.

.. [*] Compare *An Intelligent Woman's Guide to Socialism and
       Capitalism*, Bernard Shaw, Constable & Co., Ltd., 1928.

If a debtor refuses to pay his debts, the creditor
can invoke the superior power of the law to make
things even more expensive for him. But, if a debtor
nation refuses to pay its debts to a creditor nation,
there is no law about it. It is in the end a question
of force of arms. If the creditor nation, by tariffs
and the like hinders repayment and is unable to
compel the continued recognition and growth of
the debt by force of arms, sooner or later on a
favourable opportunity, the debt will be repudiated.
So the policy of the industrialised nations calls for
an ever increasing scale of armaments at once to
prevent debtor nations from either repudiating or
attempting to repay in goods their past debts!

.. page 87

Now as an initiatory step to develop virgin terri-
tories, and to provide them with the capital required
to produce the commodities we need, in exchange
ultimately for their products, the policy may have
once been admirable and beneficent to all parties
concerned. But for nations to expect to be able to
live upon one another by this exchange is merely
to try to put the engine where the starter ought to
be. The countries which we and other industrialised
nations have helped to develop are growing up.
They are consuming more and more of their own
produce and manufacturing more and more of their
own capital. Even before the War it was obvious
that something fundamentally was wrong with the
system that apparently forced the older countries to
neglect their domestic economy to a dangerous
degree, in order to export abroad vast amounts of
wealth on loan. That war is the only logical end
to the continuance of this system must now be
obvious even to the most short-sighted.

But those in charge of our national destiny are
not short-sighted. They are blind. as Nelson was,
in one eye, capable of seeing only what they wish
to see. Their eminence in affairs is due solely to
their single-eyed devotion to the ruling passion, the
problem of how, in these fecund days of science,
fast enough to convert the wealth that perishes into
debts that endure and bring in interest. Two-eyed
people may well tremble for the future of civilisation
at their hands.

.. page 88 in 2

Thus, in international relations. the fundamental
antagonism between the demands of a scientific
civilisation and the age of plenty it would inaugurate,
on the one hand, and the older mentality and systems
of life arising out of the principle of scarcity, on the
other, it more uncompromising and clearer even
than it is in domestic affairs. It may be put in a
nutshell. As in an age of plenty there need be no
debtors, so there need be no creditors. The world,
if it chooses, can pay its way as it goes. As wealth
is made more and more plentifully, the source of
livelihood of the wealthy from lending and hiring
it must tend to dry up. The millennium offered
by science is not one in which overwork, under-
feeding and overcrowding will have come to an
end, because of the dizzy virtues of compound
interest, by a sufficient accumulation of debt, but
rather one in which all are economically free so that
none are compelled either to borrow or be hired
out to labour for another's gain.

.. page 89 in 14

The existing plutocracy, or aristocracy of creditors,
can only be a transitional stage between the old and
the new. It it far less securely established than the
old landed aristocracy, which it so ruthlessly displaced.
For the world cannot permanently be kept in poverty
by financial restriction of production in peace time,
and allowed only fully to produce for destruction in
war. The party economics, whether of Capital or
Labour, will have to give way to a national and
international economics that does not end by
defeating its own purpose. Alternatively, it would
seem, the white race must destroy itself by internecine
struggles on an ever increasing sca1e or destructive-
ness and hand on the civilisation they have
inaugurated to whoever may then be left alive. It
would seem unlikely, now that the principles and
practice of science are so widely disseminated, that
the world could ever wholly revert to an unscientific
economy.
