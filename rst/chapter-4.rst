.. page 44

IV. VIRTUAL WEALTH
------------------

IF ANY reader is interested in curiosities, he should
look at one of the privately issued bank notes that
have so quietly taken the place of the well-known
Treasury notes or "Bradburys" issued during the
War. He will notice that the King's head no longer
appears upon it, and he should read, mark and
inwardly digest its legend.

   "Bank of England. I promise to pay the Bearer
   on Demand the sum of One Pound. For the
   Covr. and Compa. of the Bank of England."

(Compa. in full is Compagna, the company being
originally Italian.) If his curiosity lead him further
to present it at the Bank of England and demand
One Pound for it, as the Bearer it clearly entitled
to do, the Bank in full legal satisfaction of his demand
would presumably hand it back to him! He has
a legal claim to one pound's worth of wealth which
he can exercise by buying anything on sale to that
extent, and handing over the note as legal tender
for the debt incurred. So what the Bank so glibly
promises, they leave to the public to perform.

.. page 45 in 3

Thus has disappeared the last pretence that money
is a debt against the issuer of it, instead of a mutual
interindebtedness between the individuals of the
nation in which it is legalised, to the very great
clarification of the whole subject. The fine cobwebs
of distinction, without any real differences whatever,
between one kind of money and another woven
by the numerous apologists for its private issue,
do not in the least affect the overruling consideration
that all are of equal effect on prices, that is to say,
on the value of the money in goods.

What has been termed in this book cheque money,
non-existent money, or money owned as bank
balances in current account, is usually loosely termed
"bank credit". But just as a loan is genuine or
fictitious according as the lender does or does not
give up what the borrower receives, so credit may
be genuine or false, and in no true science would
the use of a well understood term in a totally new
and vitally different sense be tolerated. That is
the characteristic of charlatanry, fraud and deception,
in which the language is intended not to elucidate
but to mislead.

Some writers have honestly argued that banking,
meaning the private uttering of money, should be
as free from government interference and regulation
as any other business. Others, having studied bank-
ing le(d)gerdemain, are similarly anxious to have
it extended without limits, to selling articles below
cost price and doing for nothing all the expensive
public enterprises which we cannot afford to pay
for, and so avoid the unpleasant necessity of anyone
having to give up anything at all!

.. page 46 in 2

The fallacy of all this is that the value of every-
one's holdings of money is directly affected by every
new issue or cancellation. Modern business is
nothing but a tangled network or mutual contracts
involving indebtedness on the one side and claims
to be paid back in the future on the other. Every
such contract is affected by alterations in the value
of money in goods, or prices. The State insists
on just weights and measures. The pound avoir-
dupois may not weigh 16 ounces one day and 6
of the same ounces another, and then go back again
to 16, by people tampering with their weights or
weighing machines. But during and after the
War 12/- out of everyone's £ was taken and handed
to other people, so that the £ was worth only 8/- of
the former value. Now a debt of some
£8,000,000,000 incurred during the War, is, by
the attempt to bring back the pound to 20/- pre-War,
in process of being converted to a real debt
of approximately double what it was in terms of
the pounds in which it was borrowed.

.. page 47 in 13

This, if you will, is straining at a gnat and swallow-
ing a camel. It reduces to a hypocritical sham all
the enactments and machinery to secure just weights
and measures, - the solemn standardisation by the
National Physical Laboratory, and the little army
of inspectors going round stamping and marking
every quart pot or other device by which things
are bought and so1d. For a variation in the value
of money is tantamount to a similar variation in
*every* unit of weight or measure. The State is vitally
interested in maintaining an honest monetary unit
as it is the basis of all commercial and business
integrity. If variations occur on a serious scale, the
effect is to make any long term business contract or
obligation impossible.

Nothing emerges so clearly to-day as the diametric-
ally opposed interests of two powerful classes of
people in attempting to alter the value of money,
which on a universal scale corresponds exactly to
what would happen if weights and measures were
allowed to be tampered with. Buyers would want
to make the yard, the pound or the pint larger
and sellers smaller in their own interests and producers
ever seeking "cheap credit facilities". So we have
industrialists. What they mean is that they want
the banks to issue new money abundantly and lend
it to them at low rates of interest. They get the
"something for nothing" on loan, merely paying
interest not the capital sum they would otherwise
have to raise. They profit twice because what they
have so got for nothing, being taken out of the
aggregate value of other peoples' money, causes all
prices on the average to rise, and, when they come
to sell the goods which the new issue has enabled
them to start producing, over and above their
ordinary profit, they get more in money for them
than they have paid as wages and services for their
manufacture. "Cheap credit facilities" is thus, in
plain English, a double theft.

.. page 48 in 6

On the other side, there is the powerful "rentier"
class, whose income is derived from fixed monetary
payments, such as interest on debts, which, measured
in goods rather than in money is directly proportional
to the value of money. These demand "sound
finance", which means they want the banks to
stop issuing "loans", to call in those they have
issued, in order to lower prices and raise the goods
value of the £.

In general it may be said that though variation
in the value of money in either direction is an equal
injustice morally, of the two, that done to the
creditor class by debasing the currency is less injurious
to the community, than that done to the debtor
class by attempting to raise the value. Because
the world's work is done by the debtor class, which
embraces Labour and productive industry, who are
in debt for the capital organs of production, and
forced to pay rent and interest and in general to
enter into long term contracts for definite money
payments, irrespective of its value. The creditor
class econumica1ly speaking is a burden rather
than an asset. Under deflation, as now, the dead
hand of the past, already heavy by reason of failure
to provide adequate sinking funds to extinguish the
growing indebtedness of the nation to individuals,
is apt to become too grievous to be borne. France,
Italy and Belgium have all stabilised their currencies
at a fraction of their former value to lighten their
War burdens, while Germany, Austria, and Russia
debased their old currencies to zero, so wiping out
their monetary indebtedness, and have started
afresh with new currencies. We are the only nation
heroic enough to try and double our burden.

.. page 49 in 4

The professional classes, who are remunerated for
their services on monetary scales fixed largely by
custom, suffer with the creditor class, if prices rise
and the goods value of money falls. But, in the
balance, more actual interference with and damage
to the life of the community results from deflation
than from inflation. Nevertheless, to most people,
low prices will seem a public benefit and high prices
a public evil. The really vital consideration is that
when prices fall below cost of production the flow
of wealth ceases, whereas it is stimulated by high
prices. For this reason in spite of the appalling
conflict in progress, and of the shortage of foodstuffs,
and in spite of grave injustice to many people, the
economic life of this country during the War was,
as a whole, ampler and healthier than it was before
or than it is now.

If money were issued in the only correct way by
the Nation, the services of the banks would be
entirely superfluous and the function of Parliament,
as in the maintenance of other standards, purely
formal. The whole interest of banking is in incorrectly
issuing money, and their skill, for what it is worth,
is in subsequently "correcting", so far as may be
possible or desirable, the inevitable consequences of
their own action. They themselves produce the
industrial evils which they are always, as now, being
called in to prescribe for and cure, much as Rasputin
is reputed to have done with the last Cesarevitch.

.. page 50 in 7

It is impossible under the system for the nation
ever to have the correct quantity of money. It is
always being "corrected". It was deluged in it
after the War and now is being subjected to the
last agonies of thirst for it. It is highly significant
that the most remarkable consequence of the system
is that banking continuously flourishes. Superficial
observers and professional apologists for its methods
would have us believe that it is not science, invention,
business enterprise, technical skill and efficient
production which is the case of the growth of
wealth, but our peculiar system of "banking".

The casual observer, judging from the impoverished
state of the majority of the community, the mean
way in which they live and work, and the intense
and increasing struggle they have had in the past
century to preserve human characteristics at all,
would scarcely conclude that the trouble for genera-
tions is not how to produce but to distribute wealth
for consumption. He would be also quite at fault
in concluding that the value of money is natura1ly
a very uncertain and variable quantity.

What varies like a concertina is the total quantity
of money, not the aggregate of goods this quantity
will buy. The goods value of the £ varies violently
because this aggregate is divided up among a con-
tinually varying number of pounds, thus making
the value of each larger or smaller. The aggregate
goods value of all the money in the country is far
from being a capriciously varying quantity. It is,
by its nature, a very conservative quantity, changing
only gradually and regularly.

.. page 51 in 5

It is convenient to have a term to represent the
aggregate of goods in general which all the money in
a country will buy at the price level prevailing. The
author calls this *quantity* the "Virtual Wealth" [*]_
of the country for reasons about to be explained.
But it is to be at once understood that though this
measures the quantity called the virtual wealth, it
is not the virtual wealth and it does not explain
it. An arbitrarily fixed weight of gold measures,
or used to measure, the value of the £, but it does
not explain it or throw any light on the question
as to how many pounds of this value can be issued
in a country. Whereas the virtual wealth is a con-
ception that really does explain as well as measure
the value of money. The quantity is a definite
quantity of goods, viz. that which could be bought
with all the money if the prevailing price level did
not change, but it is not a positive or existing quantity
of goods at all. It is a negative or owed quantity.

.. [*] *Wealth, Virtual Wealth and Debt*, F. Soddy, 1926, Allen and
       Unwin, Ltd.

The virtual wealth is all the wealth the people o
a country voluntarily *prefer to give up*, though entitled
to it, in return for money claims intrinsically worth-
less. If the money is honest and of unchanging
value, they do not realise that they have given up
anything. The paper or bank money is as good
as goods to them and they appear to have just the
same whether it is in money or goods. But that
part of their possessions which is money has no
existence in fact. The value it represents was
obtained for nothing by those who issued the money,
and, except in the case of coins made of the precious
metals, it is not preserved. It is owing and not
owned. Hence the term "virtual", meaning in
effect, not in fact.

.. page 52 in 3

We may put it this way. Everything in existence
that has a monetary value, including the security
or cover deposited by those who have borrowed
the money which the banks have issued, has owners
already. The virtual wealth is the amount owed to
money owners and there exists nothing not already
owned behind it. What is behind modern money
is not the cover or securities held by the private
firms who issue it. That merely secures their financial
safety. It would be quite unnecessary in a national
monetary system. There is nothing behind the
national debt except the right of the government
to levy the taxes, and money is debt owed and
owned - both ways as between the individuals of
the nation. What is behind it is the necessity of
the people to give up wealth equal to it in value,
and this wealth-given-up is termed virtual wealth.

The gigantic interests in the private issue of money
have always pretended it is the public that insist
on there being something behind paper and credit
money. But during the War, the change from
gold coin to paper was effected without the public
being in the least disturbed. In fact it may be
said that they welcomed the change. These interests
are always trying to persuade the public of the
unsoundness of any kind of money they have not
the issue of. But to any impartial person forming
a conclusion from the evidence, the present system
must appear as fundamentally the worst and most
unsound monetary system the world bas ever known.

.. page 53 in 5

Even to those who know nothing about money,
it must seem absurd not to be allowed to make
the things we can make and do want because we
cannot make gold or find it fast enough to keep
pace with the growth of productive power. As it
is, as soon as the gold is dug out of the earth in one
place it vanishes into the bank vaults in another.
No one now ever even sees it. It would not be
required for internal use at all under a national
money system. It could be entirely reserved for
correcting the balance of trade indebtedness as
between one country and another.

Gold is in all respects about the worst commodity
to choose as a money standard. Its value, relative
to the commodities used up in living, varies enor-
mously and capriciously by reason of the spasmodic
nature of the discovery of gold-fields, the vast altera-
tions which science is making, as in every other
industry, in the technique of gold-winning, - such
as cyaniding, dredging and the like, - which are as
spasmodic as the discovery of gold-fields, and the
totally inadequate quantity that exists for the
purpose, needing the sort of dodges we have been
examining to "economise" the use of it. Whereas,
from a national point of view, economising in
money leads merely to the economising in production
and the artificial perpetuation of poverty. In times of
prosperity when more money is required, gold, being
an intrinsically valuable commodity, is hoarded and
used in jewellery and in times of depression the
hoards reappear and the jewellery is melted down
just when less of it is needed for the currency.

.. page 54 in 5

The War opened our eyes to many strange things.
It used to be thought that, for money to circulate, the
confidence of the people was necessary. Confidence
has merely relative application as between good
and bad coins or notes. Now that money is
neither coins nor notes, for the most part, but an
arithmetic sum, and all is equally legal tender, the
peoples' confidence can unfortunately be dispensed
with altogether. While the currencies of Germany,
Austria and Russia were being hourly debased,
during the War, by the issue of new money on a
gigantic scale, always someone had to own all
the money there was, whether they wanted to or not.
The foolish ones were left with it, and the sharp
ones instantly exchanged it for goods and borrowed
what they required from the foolish, knowing that,
when they came to repay it, principal and interest
together would be worth less than the principal
was when they borrowed it. All this is merely
relative as between one individual and another.
It has nothing to do with what gives the money its
total value, that is the virtual wealth.

Peoples' needs are not for so many pounds, shillings
and pence, but rather for such sums as will enable
them to buy what they need. When there is more
money and all prices rise, everyone, however much
they may object, is practically forced to keep more
ready money than before, if their lives are to go on
as before. True, the virtual wealth might suffer
some temporary reduction, and prices might rise
without any increase of money, if sufficient numbers
had reason to believe an inflation was impending.
But it is hardly likely that it would be appreciable,
having regard to the immense quantity of money,
as worked out at the end of the chapter.

.. page 55 in 2

The conclusion from all this is that people have
to be made more prosperous, or become more
numerous, to be able to afford to keep more money,
and for it to be possible to increase the quantity
without debasing the value. The normal condition
for people to want more money, in preference to
what money will buy, is that there should be goods
enough to find a market at a constant price-level,
as the demand for them is increased.

This view of money has obvious resemblances to,
and differences from the so-called "Quantity Theory
of Money", [*]_ of which it may be considered a
development. This states that "prices must, as
a whole, vary proportionally with the quantity of
money and with the velocity of circulation, and
inversely with the quantity of goods." It is, of
course, a definition rather than an explanation of
price, which brings in a number of unnecessary and
quite indeterminable factors. Expanded, it states
that the average price multiplied by the quantity
of goods purchased in a certain time, that is, the
total money exchanged for goods, is equal to all
the money multiplied by the number of times on
the average in which it is exchanged for goods in
that time. This is two indefinite ways of saying
the same thing about two definite things, for the
quantity of money and the average price-level, or
its general goods value, are both of them more
definite than "velocity of circulation" or "quantity
of goods".

.. [*] Irving Fisher, *The, purchasing Power of Money*, p. 18, the
       Macmillan Co., New York, 1922.

.. page 56 in 2

When the enquiry is not how the wealth produced
is distributed between one individual and another,
or between one class and another, but the fundamental
general question as to how production and distribu-
tion interlock, the simple exchanges of property
between one individual and another do not count.
The circulation of money proper and the sale of
goods may be confined to the exchange for money
of labour and services by payments of wages, salaries,
dividends and the like for production (resulting in
increase in wealth), and the exchange of the money
so earned or distributed for the new wealth produced
(resulting in its distribution). It should have been
obvious that no mere algebraic relation between
two ways of stating price, or the value of money,
will explain it apart from the physical factors, such
as the time required to produce wealth. This is
a separate enquiry.

The only genuine result of the quantity theory
was the somewhat vague conclusion that "if other
things did not change" prices vary proportionately
to the quantity of money. The "quantity of goods"
in the quantity theory is really the virtual wealth,
and, if *that* remains unchanged, prices are then
proportional to the quantity of money. This, of
course, is also two ways of saying the same thing if
we mean by virtual wealth all the goods all the money
would buy at the price prevailing, but not if we go
behind the measure of virtual wealth and enquire
why people *prefer to give up* this quantity of goods
for money. The quantity of money can only be
increased without depreciating its value by people
wanting more money, meaning, in preference to
more goods.

.. page 57

To issue money by fictitious loans to "stimulate
production" is to increase immediately the con-
sumption of goods, but the production only after
the interval required to produce them. With the
best will in the world, agriculturists and indus-
trialists cannot immediately increase production
when they are granted new money as bank "credit"
for the purpose. There is a definite interval between
initiation and achievement, which is in fact the key
of the whole problem, and it has been hitherto
completely ignored.

Consumption, during the time-lag, is increased
without any increase of production of finished wealth.
This results at once in a shortage of goods for sale,
rise of prices, - so that the new quantity of money
only buys as much as the smaller quantity did before,
- and a drain of gold to pay for additional imports
to make good the shortage. This is followed by
reduction in the quantity of money again as the gold
basis of "credit" is reduced, and a long and bitter
struggle to bring prices down again and to get
back to the level of production we started from.
This, in brief, is why it is worse than useless to allow
banks to utter money. The money is required after
production is matured, not as a stimulant, and at
that time the services of a bank are superfluous.
The function of the banks is to lend money, not by
pretending to lend it really to create it. If their
operations were confined to lending money, all the
rest would be simple enough.

.. page 58 in 2

Let us suppose the country is pursuing its peaceful
avocations, every man busy with his own affairs,
and nobody anxiously awaiting some change of
financial policy, such as occurred in 1921, from a
mad inflation to a madder deflation, or such as
seems in the opposite direction to be foreshadowed in
the near future by the financial medicine men to keep
the patient alive. That is to say, we will suppose the
virtual wealth of the country to be reasonably steady
and consider how much the quantity of money must be
abruptly varied to cause a perceptible increase in the
index number that measures the average price of goods.

We take, for the purpose of illustration, the good
round sum of £2,400,000,000 as representing the
average total money in the country since the War.
This is, conveniently, as many millions of pounds as
there are tenths of a penny in the pound.[*]_ We
can at once say that ten million pounds more or
less will only affect average prices one penny in
the pound, a change of less than one-half per cent..
and probably quite imperceptible to the ordinary
man. To cause a variation in the index number
of 10% an issue or destruction of no less than
£240,000,000 would be required.

.. [*] I wish to express my ackowledgments to Lt. Col. D. W.
       Maxwell for this cxcellent way of elucidating the question, and
       for other benefits derived from the persual of a book by him
       in manuscript before publication, which he sent for my comments
       and criticism.

       [I learn that the book is shortly to be published under the
       title: *The principal Cause of Unemployment*. - F. S.]

.. page 59 in 4

It is not denied that there may be some slight
periodic variation of the virtual wealth. But the
present system of keeping money tight creates and
exaggerates it to an absurd degree. Free money
from private manipulation and any real residual
effect of this character could probably be easily
neutralised by intelligent collection of taxes and
their expenditure.

Once destroy the power of banks and private
financial people to issue money, and confine their
operations to legitimate transactions, and there
would not be the least difficulty in maintaining the
index number so constant that no one could detect
its variation. It is not a question of knowing the
correct amount of money to a million or even to
ten million, for such sums are insignificant in their
effects. But so long as private people can get money
created for them and destroyed again when they
have done with it, money must be capricious in
its value and business a game of chance. The hard-
working and efficient are thereby reduced to inferiority
in comparison with the lucky gamblers, and those
who do the real work of the world are at the mercy
of wreckers and ghouls. Modern money, the vitally
necessary internal national debt between the indi-
vidual citizens in the community, to enable them
to exchange their goods and services, is treated as
an affair between some smart entrepreneur and a
bank manager to be issued or withdrawn according
to an agreement between them behind the closed
doors of a private office.

Democracy, no doubt, had to make many mistakes
before it learned the art of government. But surely
this elementary howler in regard to the nature of
a money system is, of itself, completely sufficient to
account for its failure.
