.. page 60

V. HOW A NATION CAN BE MADE WEALTHY
-----------------------------------

WE WILL assume, though it it yet far from being
the truth, that the nation does want to become rich, in
the sense of increasing the general use and consump-
tion of wealth, as well as its production, to the limits
which the state of knowledge and the industriousness
of the people allow. How is it to be done? The problem
is easier to grasp if, in the first instance, we leave
money out and consider the physical necessities.

.. page 61 in 16

The first and, to most people, surprising condition
is real abstinence from consumption, and the expen-
diture of the wealth, that otherwise people would
consume on their private wants, in building or
tuning up the economic system to a higher duty.
This is by no means the same sort of abstinence as
is preached, not as a means to an end, but as a
permanent virtue by the good people who have a
foggy idea that if we do so long enough we shall one
day all be able to live like camels on their humps,
or, by the economists, upon one another's debts.
No accumulation of wealth, but a permanent and
abiding increase in its rate of production and con-
sumption is the object. Consumption *must* be
increased as soon as the new rate of production is
achieved, or the last state of the nation will be
worse than the first. More money is needed to
complete, not to start the process.

The problem, though not quite the same as in
Russia, which has necessitated the "Five Year
Plan", - because in industrialised countries there is
now a redundancy both of unemployed labour and
capital, - is nevertheless similar. Before the future
flow of revenue for consumption can be permanently
increased, there must be a permanent irrecoverable
expenditure in the manufacture of "fluid" capital,
which includes all the semi-manufactured goods in
all the intermediate stages of growth or manufacture
between the seed-time and harvest of agricultural
and industrial production. The point seems trifling,
but it is the key to the problem of how money
ought to be issued.

To every individual manufacturer or agriculturalist,
this initial expenditure will not appear to be irre-
coverable, because he looks forward in due course,
before retiring from the business, to completing and
selling the fluid stocks as production matures, or
to recovering them in the price, if he sells his business
as a going concern. But from the community's
standpoint the reduction of the economic system to
its present poverty level is not in contemplation,
and hence they are irrecoverable. We have had
enough under private money of boom followed by
slump, the laborious effort of tuning up production
to a higher level, followed by the turning off of
workers to fend for themselves, and cessation of
further production until the excess stocks are finished
and sold off.

.. page 62

The fact that their fictitious loans can so be repaid
is the attraction to the banker. He will not usually
create "credits" for permanent capital expenditure,
as that is irrecoverable expenditure. He knows,
when the money is expended upon increasing the
production of consumable goods, that they will
mature in due course, and what has been put in
can be drained out. In this his attitude is just the
same as the individual producer. But it is no use
initiating a water supply system at all unless the
*quantity* of water necessary to fill the pipes is per-
manently lost to use and buried underground along
with the pipes. The actual water is always in flow
through the pipes but that *quantity* is always irre-
coverably there. No one would consider it good
sense always to be emptying the pipes to make sure
it was still there for use if people should ever be
dying of thirst. So it is not the least use our building
up an economic system to deliver a greater flow of
goods, if the fluid stocks in the system are to be
at the mercy of the banker draining them out again
to pay back short term fictitious loans, which are
then re-lent to another party to do the same with
over again. People do not live by such fits and starts,
though an Irishman is reputed to have tried it on
his pig to get streaky bacon, but the pig died.

.. page 63 in 6

If, on the average, it takes half a year from the
time of commencement to the time of completing
production, and we wish to double permanently
the wealth of a country, so that, after half a year,
it can produce and consume just twice as much as
it does at the start, then we have *irrecoverably* to sink
half a year's production at the initial rate. We
may suppose the nation as one huge factory increasing
its staffs by taking on additional employees on a
certain day and, from that day, doubling production.
Half of this is the normal former production which
pays for itself by the selling of the product as it
matures. The other half has to go on for half a year
before the product becomes ready for sale, *after*
which time it also can be made to pay for itself
by the selling of what is produced. The extra
expenditure has to be borne for half a year. Those
engaged on it have to be paid and are, all this time,
consuming finished wealth. Where is it to come from
and who is to pay for it?

Paid for it assuredly will be, if not honestly, then
dishonestly out of the virtual wealth, by a corres-
ponding reduction in the goods value of the unit of
money. If it is not honestly paid for, all hope of
maintaining an honest invariable money standard
may as well be abandoned at once. Whereas if
the expenditure is properly accounted for, this
will be the natural result, instead of being, as
now considered, an ideal impossible to realise in
practice.

.. page 64 in  8

The widespread ruin and deadlock resulting from
variations in the money standard should of themselves
be sufficient to condemn any system of economics
as false which depends upon such variations for
adjustments between supply and demand. The
solution of the economic tangle of the day lies in
avoiding the petty tricks which pay for part of the
costs of industry out of the general value of money,
and in making it the first essential to the solution
that the value of money shall not be debased for
this end. Then we shall find that our economic
system is so stubbornly inflexible that there is only
one answer to our problem.

Those engaged in initiating the new scale of pro-
duction must be paid for by others abstaining to
the same extent from consumption over the period
required for the new sca1e of production to mature.
Then, and continuously afterwards, every one may
consume on the average at the new and increased
rate of production. But for this to be possible, the
money in circulation must then be correspondingly
increased. If this is not done the whole purpose of
increasing production is frustrated. The new sca1e
of production cannot possibly be distributed by the
old quantity of money, if its value is to remain
invariable. The quantity of new money required
bears a similar proportion to the amount already in
circulation as the increase of production to the former
rate of production. This quantity, although a tidy
sum, even in national accountancy, is insufficient
to pay for the expenditure in initiating the new
rate of production. It would be the same if money
took on the average as long on the way from earning
to spending as goods take in making. Whereas
probably it does not take a fifth as long.

.. page 65 in 5

This may be elucidated by the simple diagram
at the commencement of the book (p. viii). The
economic process as a continuous cycle of production
and consumption may be represented by a ring
divided into two halves, the left-hand half referring
to consumption and the right-hand half to production.
The clock-wise circular arrow represents the really
important circulation of money, as distinct from
incidental changes of ownership among individual
consumers or producers, respectively. At the top of
the ring the passage of money from the consumer's
to the producer's side symbolises the buying of new
products for consumption and use. At the bottom,
the passage from the producer's to the consumer's side
symbolises the payment of wages, salaries, dividends
and the like, for all the services, real or imaginary,
in production, - that is for putting new wealth into
the producer's side of the system.

.. page 66 in 19

The professional economist, but not often the lay-
man, quite understands that, in the ultimate analysis,
such payments account for all production of wealth.
Though any one farm or factory may have large
expenses in buying materials, these costs arise as
wage, and similar payments made in the other
factories, fields or mines, from which the materials
are derived. But what the professional economist
does not seem to have yet appreciated is that wealth
is now an artificial human product made by
scientific methods with ever-increasing certainty and
efficiency, and the older confused ideas derived from
the age of scarcity are entirely upside down. Scien-
tific men, having successfully solved the problem of
wealth production may be credited with having
contributed something to the understanding of the
real nature of wealth, and to them, as indeed to
anyone who thinks about it, the co-existence of
poverty and involuntary unemployment is a suffi-
cient indication that, for a long time past, the orthodox
economist has been somewhere vitally at fault.

.. page 67 in 30

The diagram of the economic system, simple and
crude though it be, makes clear certain features
which are often overlooked. Consumers have to
be provided with money as much as producers for
the system to work properly, the one providing
the other in an endless circulation. Inability to
distribute goods is as much a cause of poverty as
inability to make them. Or, to put it the other
way, poverty is due to inability to produce sufficient
goods for national requirements, and this inability
may be real technical inability or it may be purely
artificial, due to the inability to distribute what
has already been made. For many a long day now
the poverty of this country has been overwhelmingly
artificial. People who are always insisting on the
necessity of cutting down wages, economising in
consumption and increasing production, as an
economic general principle rather than a temporary
expedient, are trading on the ignorance of the public
of this vital feature of the process. If they are not
ignorant themselves, then they are merely the spokes-
men of the creditor or rentier class, trying to reduce
the price level and increase the real burden of the
past indebtedness of the nation. Elsewhere they
seem to see through these partizan policies more
easily than in this country, where the public always
seem ready to endure any hardship and to sanction
any damage to its trade and industries in the interests
of "sound finance" and "raising British credit to
a higher level." This is at best only one side of the
difficult questions which arise if we allow the value
of the monetary standard to vary.

Under a constant standard, the economic system,
once got going, is stable or conservative and self-
regulating. For if consumers buy more than the
usual amount they get short of money and less able
to buy, while producers having more money than
usual are the better able to make up their deficiency
of goods. The sum total, goods + money, remains
unchanged for each side of the system separately,
and consumption balances production. In initiating
the system, or what comes to the same thing,
increasing its capacity to produce, more money
has to pass from the consumption side to the produc-
tion side than through the regular channel at the
top of the ring, by a method which does *not* take
out goods equal in value to the money entering.
This is symbolised by the bye-pass marked invest-
ment, and the reason has already been explained.
This money going in at the bye-pass and coming
out at the bottom of the ring puts goods into the
productive system without taking them out, with
the consequence that the quantity of goods in the
production system continually increases while this
investment goes on.

.. page 68 in 7

After the time-lag, corresponding to the natural
period required for production to mature, the rate
of distribution of wealth at the top of the ring can
be permanently increased without any further
investment being required. We now come to the
whole crux of the operation. If, *then*, consumers
are not provided with the additional money to
purchase the additional flow of goods, the latter
cannot be sold. It is a glut. Industry is overstocked
with goods and perforce must reduce production.
Not only the additional workers taken on must
be turned off again, but also some of the original
ones, until the extra stocks of goods, put into the
system by the investment process, are distributed at
the old rate. Lean years would succeed the fat
ones, not because of any failure in production, but
because of the failure in distribution, even under
an invariable money standard, unless new money
be issued to buy the increased harvest.

.. page 69 in 20

It is transparently absurd to expect to reap a
harvest before sowing the seed, and it is as absurd
to issue new money in order to increase production.
It is easy to see what must occur if the attempt of
the system to make good the error by rise of prices is
prevented or "corrected". It corresponds, on our
diagram, to the creation of more money in the
producer's side of the system, which, being paid
out as wages and almost at once exchanged for
finished wealth, drains the stocks of the latter out
of the system, putting in merely unfinished inter-
mediate wealth at the first stage of manufacture.
This deficiency of finished wealth is not temporary
but permanent. The only way it can ever be made
up is by the rise of prices to consumers before
wages, etc., are correspondingly raised to workers for
producing. If the attempt is made to purchase the
deficiency of goods from abroad, it drains the gold
out of the country to pay for the additional imports,
and, if money is on the gold basis, the amount of
new money so issued has to be called in again.
This way of "assisting" industry, by the double
theft of "cheap credit facilities", is thus like nothing
so much as the way of a cat with a mouse. It is an
attempt to reap before sowing, and as the law has
always recognised for the individual counterfeiter,
is not mere theft of the amount forged, but
high treason against the economic life of the
nation.

.. page 70 in 23

So we reach the *reductio ad absurdum* of the con-
fusion between national and individual wealth.
Wealth, to the nation, something for use and con-
sumption in the maintenance of life, is to the indi-
vidual wealthy person something to lend at interest
and a permanent source of income to the owner
of the debt. Money is individual wealth. It is
therefore something to lend at interest, and it would
be "wasteful" not to take advantage of this source
of "wealth" in the issue of it. The lending of
money at interest is the legitimate business of banks.
Therefore the banks are the profession to whom
naturally to entrust the issue of money, as they
know how to make use of this source of "wealth"
so that it is not "wasted". The result is that the
community can only owe one another for the goods,
which their money is the equivalent of, on the
condition that, and as a mere after-consequence of,
certain private individuals being willing to pay the
banks their rate of interest upon the money debt the
individuals of the community owe one another.
Some private party, before any new money can be
issued, has to pay to some bank interest on the
debt the individuals in general of the community
owe amongst themselves.

In one respect, counterfeiting, if it could not be
detected and if it could be kept within the right
limits, would be preferable to this system. For as
is only now becoming understood by people not
professional financiers, the nation's money, is a mere
bye-product, as it were, of money-lenders lending
what they have not got to lend and always very
jumpy in consequence about getting *repaid*. Money
is not regarded as a national debt at all, but a purely
private one. To satisfy those who pretend they
have lent it, it must be periodically drained out
of the productive system, and the community
instead of a live industry it left with the dead corpse.

It is most satisfactory to the banker. He creates
and destroys the nation's money as though it were
his own. He can make the yard, the pound weight
and the gallon all larger or smaller in consequence.
He makes open mock of the law and a failure of
the fine gift of science to humanity. It is not enough!
There must be no one to challenge the new power.
The King's Head has to be defaced from the national
money and replaced by his absurd "Promise to
Pay". Of all forms of government to which their
ignorance has condemned the wretched peoples,
surely democracy in its present plight is the silliest
and the worst.
