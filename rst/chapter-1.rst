
.. page 9

I. THE NEW ECONOMICS
--------------------

THE AGE in which we live is scientific. Four out
of every five of the people now alive in this country
owe their existence to science and would starve if
it were to revert to its former regime. Dangers
are crowding thick and fast upon the scientific
civilisation. Its problems call for fearless and
original scientific thought if it is to survive and
triumph. It has been left too long in peril of ship-
wreck - at the mercy of medieval and obsolete
ideas.

.. page 10 in 11

The harnessing of the inanimate power of fuel
and waterfalls to do the work of the world has
abolished the iron law of scarcity of Nature. Poverty
has been for long in this country a purely artificial
condition, which it is becoming increasingly dangerous
to enforce. The engine of modern production is
so powerful that, when it gets going, those in charge
become afraid of it and of what it will do. Instead
of putting in the clutch and letting it do the world's
work, of which there is, to be sure, a plentitude
crying out to be done, they get scared and turn off
the fuel or the ignition. Absorbed in frenzied efforts
to prevent it from stopping and to get it to start
up again, the world is in danger of forgetting
altogether what production is for, at any rate in
peace time.

Fear of what science is going to do to the world
is, from the standpoint of those who, hitherto, have
had the direction of it, only too amply justified.
For their sort of world and science cannot co-exist.
In the past ages of economic scarcity as the law of
life, civilisation was of necessity based upon some
sort of slavery, however ameliorated or disguised.
In an age of plenty, if it is ever allowed to come,
that slavery would be transferred from men to
machines. To resist it is but to make men the
slaves of machines, and the world their sport.

A moment's thought is sufficient to reveal that
any condition of universal abundance must be
something quite different from what prosperous
people to-day regard as affluence. It requires a
good deal more than a moment's thought to form
any picture of what it would mean. But the longer
the possibility is contemplated the clearer it must
become that *nothing* could remain unchanged in
such a world. In that is the key to the riddle. It
explains at once the apparently stupid and wanton
refusal of nations to use the good gifts of science
in peaceful construction and the whole-hearted and
single-minded purpose with which the clutch is let
in and the throttle opened all out for destruction.

.. page 11 in 2

In the alarming and novel sort of world we live
in now it is clearly little use trying to take it at its
old valuation. One is more and more forced to
regard it as a new phenomenon. No doubt poets
and visionaries have always recognised, in a mystical
and allegorical sense, the essential unity of the life
of man and of the animal and vegetable kingdom
with that of inanimate nature, - sunshine and water-
fall, fire, tempest and the round of ceaseless change.
The fancy has come true. Life is increasingly being
bye-passed as a means of life. More and more men
reach out and consciously draw upon the abundance
of inanimate energy in Nature for the maintenance
of their lives, and are so empowered to perform
tasks formerly beyond the capacity of life in any
form.

By solving thus the problem of the satisfaction of
his needs, men are for the first time realising what
these are, - what, in fact, wealth is. How different
is the reality from the sweepings of the mouldy
ages, the pawky observations upon the interplay of
human necessity and greed, which, in these days
of artificial poverty and actual glut, still passes
as the science of wealth! This "New Economics"
is the science of wealth. The old is, rather, the
science of want.

.. page 12 in 8

Men's needs are as varied and intricate as the
network of pipes and cables, and all the appliances
connected with them, which constitute the gas,
water and electricity systems of the modern world.
But, like them, they have a common interpretation.
Their purpose is to direct energy and materials
from where they can be obtained to where they are
required, and to transform them into the qualities
and kinds capable or doing the work, performing
the services, and making the things required by the
life of the community. All the necessities of men
are, in the ultimate analysis, satisfied by the expendi-
ture or consumption or available energy. Science,
by enslaving the forces or Nature, offers freedom
to men. The gift is being vehemently and fanatically
refused, but the alternative is chaos.

This book is written in the belief that economic
sufficiency is the essential foundation of all national
greatness and progress. It is held increasingly by
those with actual experience of the effects of want.
The eugenist, in so far as he ignores this, neglects
one of the fundamental factors in his subject. Fre-
quently he appears to ally himself with superficial
people, who, being well provided for themselves,
affect to despise wealth and fear its effect on others.
The crime being perpetrated to-day is not the mere
waste, or worse than waste, of wealth, but or the
opportunity it affords to build up a type of civili-
sation nobler and more humane than was possible
in a world held in the grip of, and limited by,
want.

.. page 13 in 9

No discovery in history has been fraught with
such swift fundamental alteration to the fabric or
human society and the nature of social interrelation-
ships as the substitution of inanimate energy for
animal labour in production. At first sight it would
seem that nothing but good could possibly result
from so great a boon. The very goal towards which,
from the earliest dawn of civilisation men have
persistently striven, the lightening of the labour
involved in living, was thus attained almost miracu-
lously at one bound.

Whereas the actual results so far have been in
many vital respects nothing but disastrous. A large
and increasing proportion of the workers in all
industrialised communities are being deprived of
their livelihood and with it of their right to consume
by the growth of the new scientific methods.
Avowedly and intentionally these are adopted for
the purpose of saving labour, and they save it.
Could any result be more natural or expected?
Yet the problem of unemployment is not usually
so directly and bluntly accounted for! In seeking
the cause the world resembles nothing so much as
an ants' nest that has been sat upon.

It used to be argued that this cause could be
but temporary, as those displaced by labour-saving
machinery quickly find employment in developing
the capital required by further new inventions and
labour-saving processes. But as Ruskin pointed out,
the best part of a century ago, this is exactly like
re-sowing a harvest over and over again - 'seed
issuing in seed never in bread,' - producing harvests
increasing at compound interest, and completely
forgetting the only purpose and object for which
the harvest is grown. Labour-saving methods create
leisure. For an initial period only can the displaced
labour find occupation in creating the capital required
to develop new enterprises, because in turn as
these come into operation they displace yet more
labour, and the effect is cumulative as events
show.

.. page 14

It should be obvious, but has indeed only been
latterly appreciated, that as men lose their employ-
ment and can no longer buy the goods which they
are no longer required to make, the home-market
must continuously shrink from this cause. As
capacity to produce increases demand shrinks. It
is not enough that people urgently need the very
things that cannot be sold. There must be what
is conveniently termed "effective demand", which
means demand backed by the ability to pay, and
those whose incomes dry up by loss of employment
cannot pay.

In the era now closing, the titles to consume the
product more and more found their way into the
hands of those who owned the new capital organs
of wealth-production, whose actual consumption
needs were fully supplied, who desired to defer
their titles to consume to the future, - to lend
their surplus and create debts against the com-
munity. In this way they sought to secure a
permanent lien upon the future revenue of the
community without any further contribution to its
production.

.. page 15 in 9

For many years, now, the problem of producing
wealth has been essentially solved. There is no
difficulty whatever in producing wealth in accordance
with national requirements and demands, or in
exchanging it by way or barter with that more
favourably produced abroad. Everywhere there is
a glut of wealth, intense competition for markets,
and the conditions which, in times of peace, produce
congestion of the economic system and ever-growing
unemployment among the workers, lead also and
will continue to lead to periodic world wars. These
wars in their origin and consequences are the exact
opposite of those waged in earlier history. Never-
theless, as science ever increases the destructive
power of men, they will, if not prevented, end in
destroying the scientific civilisation altogether.

The problem that is not solved and which must
be solved quickly, if civilisation is to be saved, is
the problem of distributing the wealth that now
by scientific knowledge can be so plentifully produced,
not for the yet further increase of productive capacity
in the future, but for consumption and the satis-
faction of the economic needs of the individuals
that make the community. The industrialised and
the agricultural workers in this country, as the
statistics collected by health and poor-law officials
during last century showed, have been physically
demoralised by their economic conditions. The
military statistics collected during the War but
reiterated this conclusion, and the War itself showed,
by contrast with the superior stamina and physique
of the overseas contingents, that our people suffer
to a dangerously prevalent degree from actual malnu-
trition and economic insufficiency!

.. page 16 in 7

The unemployed, but for the expropriatory taxation
levied on the well-to-do, would starve. As it is
they only half live. They consume less and less
as a right and more and more as a charity, and
insecurity of livelihood saps their independence and
morale as poverty undermines their health and that
of their children. The submerged tenth of the
nineteenth century ever grows. Though ameliorative
legislation may have reduced the actual intensity
of their individual sufferings, the proportion affected
increases. This is occurring in an age in which
science has already abolished the physical necessity
for poverty. The penury of the masses accompanies,
as ever, and enhances the relative wealth of the
few. This is not national wealth, but the reverse.
It is only too clearly indicative of national decline
and decay.

It is always instructive first to eliminate money
altogether from an economic question as a merely
intermediate factor, and to reconsider the problem
simply in its ultimate aspects. If we do this to-day,
we shall be forced to the conclusion that under
democracy there is no effective government what-
ever. For we have on the one hand invention and
technical skill at a high water mark, never even dreamt
of in any former age, an accumulation of industrial
plant and machinery equipped with the most power-
ful and time-saving methods of production and a
highly skilled army of workers of all grades, together
with more abundant natural resources, more efficient
means of transport and communication than ever
before in the history of the world. All is, to a
large extent, idle and in decay, both men and
machinery, because of the failure of the distributory
mechanism.

.. page 17 in 4

To anyone who has ever penetrated even a little
below appearances and can understand the elements
of the principles which underly the provision of
economic needs, a people in which involuntary
unemployment, both of men and machines, co-exists
along with acute and chronic poverty and destitution
is in no sense a nation or a community but an anarchy.
Its ostensible government is not its real government.
In the most vital affairs of its economic life it is leader-
less impotent and doomed.
