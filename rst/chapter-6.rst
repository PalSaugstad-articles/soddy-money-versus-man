.. page 71

VI. OTHER PROPOSALS
-------------------

.. page 72 in 23

IT IS CHARACTERISTIC of the age that it deems obvious
and uncompromising truths uninteresting - obstacles
to be circumvented and to be overcome, limitations
to the aspirations for sovereignty of the mind over
matter, - rather than the essential framework upon
which that sovereignty depends. The great advances
in the realm of positive knowledge of the last few
centuries has still to be properly interwoven into
educational curricula as its framework, and not
merely treated as a clothes horse on which to hang
literary, mathematical and philosophical embroidery.
There is apparent in science a retrogression in this
respect, - an alarming tendency to garb plain facts
with a cobweb garment of elegant fancies and
mysticism, to meet the public demand for this
sort of dressing up. Knowing more than any pre-
ceding age as to the conditions under which we
have dominion in the world, it is being made to
appear that we "really" know nothing certainly at
all! No greater cloud of mystification and fallacy
exists than in regard to money. "Credit" is
regarded by some of the public as a sort of magic
talisman for escaping the necessity of sowing as
preparatory to reaping. This invades even "The
New Economics".

All "New Economists" subscribe to the doctrine
stated in this book in the form, in brief, that the
age of economic scarcity it past, and has already
been replaced by a potential age of plenty. It it
associated in this country with the name of Major
Douglas, whose post-War writings[*]_ opened the eyes
of many to the undeniable truth that poverty to-day
resulted from lack of distribution rather than from
any physical inability to produce what was required.
Before him, for many years, Arthur Kitson, in this
country, and Silvio Gesell, in Switzerland and
Germany, had more or less vainly attempted to
interest the general public in the vital importance
of currency questions, and the evils of the gold
standard as an arbitrary and unnecessary limitation
of the production of wealth.[*]_

.. [*] *Economic Democracy and Credit Power and Democracy*, Major
        C. H. Douglas. Cecil Palmer. 1920.

.. [*] For a coniderable litenture that has sprung up round
       these questions see *This Age of Plenty*, C. M. Hattersley.
       Appendix K.

Unfortunately, the Douglas school, under the
term "Social Credit", push the popular fallacies
that exist with regard to that magic talisman beyond
even the limits of human credulity.

.. page 73 in 6

As to the facts there can be no serious dispute.
All "New Economists" are in agreement with the
diagnosis of Major Douglas, to put it more or less
in his own words, that the consumers are becoming
increasingly less able to buy the whole potential
production of industry, and that more money in
the pockets of consumers is the remedy. But on
the reasons for this shortage of money, and the
method of correcting it, the theories of the Douglas
School seem to have no basis in fact or reality. They
argue that this is the result of a flaw in the price
system,[*]_ and that the shortage of "consumer-
credit" is the result of the piling up of "overhead"
charges for interest on, and for the initial provision
of, capital. They recommend that part of the costs
of manufacture should be met by the issue of "Social
Credit" to the seller to enable goods to be sold
below cost price.

.. [*] *The flaw in the Price System*, P. W. Martin, P. S King,
       1924.

Of course it must be understood that, if all hope
of maintaining a stable money standard is aban-
doned, it is a possible solution to remedy the defects
of the situation by a progressive and continuous
inflation and debasement of the value of money.
These reformers deny that this would be the necessary
result of their proposals, and argue that it can be
"prevented" by suitable orders or legal enact-
ments. But this gesture will carry as much weight
to those prepared to think the matter out as King
Canute's celebrated gesture to his courtiers, when,
to show them what fools they were, he sat on
the sea-shore and commanded the rising tide to
retire.

.. page 74 in 4

It is impossible to emphasise too strongly, as the
neglect of this point seems to underlie most social
and political controversy, that the production of
wealth requires consumption equally whether those
producing are producing capital or consumable goods.
In the diagram all fixed capital bas to be put into
the productive side by bye-passing money as
explained, but this capital never comes out again.
All the fluid capital in this side has to be put there
in an identical way, and though what is put
in is always coming out, a certain *quantity* of it
never comes out and bas to be treated as fixed
capital.

To expose the fallacy in the first argument, let
us suppose, for illustration, that one-half of the
money issuing from the productive system goes to
producing fixed capital, and the other half to produc-
ing "consumable" goods, that is, the only sort of
goods which the public can use or consume. Of the
money going to the consumers, one-half is reinvested
and the other half buys the goods for sale. The half
of the money suffices to buy the half of the product
which is the only half buyable. There is no rise
of prices, or flaw in the price system. Or, suppose
that, owing to the continuous piling up of over-
head charges, one-half of the money paid to consumers
consists of wages and the other half of dividends.
This alters the distribution of the product among
individuals, certainly, but it does not alter the total
amounts claimed. It may and probably does under
present conditions lead to an over-production of
capital goods in contradistinction to consumable
goods. But the vague statement that the "absorp-
tion" of the money in meeting interest and other
charges prevents consumers from buying the whole
product is not true.

.. page 75

The true reason has been given already. If, by
genuine investment, investors have enabled the
preparatory stages in the production of consumable
goods to be carried on till the product comes on the
market for sale, the money in the pockets of the
consumers being sufficient to buy only the original
product is now insufficient to buy the increased
product, unless prices fall. The orthodox experts
seem to look upon such a fall as the natural "correc-
tion", whereas it entirely dislocates the whole
community. Under modern democracies, whose
finances are a mere money-lending proposition, there
is no one whatever to issue money to consumers,
when there is more to be consumed, and it is at this
precise moment, under the cat-and-mouse system
that it is withdrawn again.

The idea of defraying part of the cost of production
by the issue of "Social Credit", or, not to be
mystical, new money issues, is clearly chimerical.
Under such a provision, the quantity of money
would increase, not as the rate of production increases,
but as the totality of goods bought and sold increases,
that is to say continuously whether production
increases or not. Once issued, and not destroyed or
withdrawn again, money goes on functioning for
ever.

.. page 76 in 6

The fallacy seems to have been in supposing that
"Social Credit", like bank "credit", when the
loan is repaid, could be cancelled when the sale
had been effected. It would be of little comfort to
the seller who had sold below cost and received a
State credit for the difference (or to whoever in the
course of trade the credit had been passed on)
to find that, having achieved its purpose, it had
been cancelled. The greatest obstacle to the cause
of currency reform is the prevalence still of
errors such as this among the reformers them-
selves.

The views of one of the chief Schools on the
Continent, that associated with the name of Silvio
Gesell, who died in the year 1930, may be
briefly referred to. They advocate a currency of
continuously diminishing value, not by the simple
method of progressive inflation, but by making it
legally necessary periodically, to affix stamps to the
tokens, much as we have to do to insurance cards,
to keep them current. They hardly seem to have
got beyond thinking of money as tokens, and seem
unaware of the existence of fictitious credit and its
natural consequences. True, the cheque system has
not abroad achieved the dominant position it has
in this country, but everywhere it appears to be
growing rapidly.

.. page 77 in 11

They argue that money has an unfair strangle-
hold upon industry, because it is imperishable,
whereas goods for the most part deteriorate. So
that the buyer has only to bide his time and the
producer is forced to sell at the buyer's price, even
at a loss. As a matter of fact, this could not occur
under a national monetary system of invariable
value. So long as there was sufficient money to
buy the whole product at a constant price level, if
one set of buyers abstained from buying and waited
for a fall of prices, another set would buy instead.
This is the same as occurs during inflation when,
as is well known, the buyer is completely unable to
resist the rise of prices. Producers are in fact given
the right to sell at a constant and invariable average
price-level. General unsaleability, in contrast to
relative unsaleability through the public preferring
other goods, could not occur. People who want
things abundant are asking impossibilities if at
the same time they want them so cheap that it
is not worth while for the producers to make
them.

So far as any open criticism of our monetary system
has been made by the official spokesmen of the poor
and exploited in this country, it has been limited to
the feature that it encourages all sorts of anti-social
gambling and activities equally with legitimate
enterprise. It has not got beyond schemes of classify-
ing "credit", to grant it to genuine home-producers
at nominal interest, and then to others at interest
rates rising with the anti-social character of the
purpose. This seems like trying to make a mechanism
discriminate morally, and is as feasible as to try and
invent a train that would only go if its passenger and
driver were of blameless character, or a weapon
that would destroy only the originators and not the
victims of war.

.. page 78 in 6

A money system should be a straightforward
mechanism, like an honest weighing machine, for
keeping accounts correctly and for the swift and
unimpeded distribution of all the community can
produce, not one that will favour anyone class at
the expense of another. The restrictions of fictitious
credit to deserving objects is, at best, doing evil
in the hope that good may come, and only ill would
be the result. Money is a binding social contract
and is no more to be insidiously repudiated than any
other business contract.

The plain man will sympathise neither with the
industrialist who, to finance his production, damages
everyone except his own class by making their incomes
worth less in goods, nor with the rentier class who wish
to make their fixed monetary incomes worth more.
The simple justice is that neither should be permitted.
and that the State itself should issue genuine money
at a rate which will keep the average price of goods
or the price index, constant from one century to the
next.

.. page 79 in 17

So called "practical" people take the view that,
unanswerable as may be the case against private
money, the vested interests of the bank in its issue
and destruction on a colossal scale are so strong, and
the powers they now wield in the community so
unchallengeable, that it is mere waste of time advocat-
ing any reform which could interfere in any way
with these interests. The view taken in this book
is that compromise on this question is *not* practical,
any more than it would be on the matter of tampering
with the standards of weights and measures. If
some people were allowed to concertina these stand-
ards to suit themselves all the others might as well
go out of business altogether. So, if some people are
to be allowed to issue and destroy money, all the
others may as well give up at once any idea of
economic independence or freedom, and hire them-
selves out to those who have this power at the best
terms they can. There cannot be two Heads in
one State and the people have to choose between
Parliament and the Banks.
